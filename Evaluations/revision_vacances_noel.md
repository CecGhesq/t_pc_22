![](https://www.gif-maniac.com/gifs/49/49357.gif)Bienvenue dans le __programme idéal de révisions des vacances__ d'un élève de terminale en spécialité Physique -Chimie   

![](https://acegif.com/wp-content/uploads/2021/4fh5wi/christmas-lights-acegif-37.gif)  

# lundi 19 décembre: 
## Chapitre 1 : La lunette astronomique
1. Relire cours :  sais-tu  refaire la démonstration du grossissement et tracer les rayons pour une lunette afocale ?
2. Faire les  31 et 33 p 452 et un grand exercice vu ensemble.
3. Convertir les unités : __Méthodes indispensables 6__ surtout sur les angles

# mardi 20 décembre
## Chapitre 2 : Transformations Acides/ bases : cours à relire ; 
1.  Faire cette activité [révision](https://labolycee.org/acide-base-cartes-de-revisions)
2. Faire les exercices 34 et 36 p 37 et et un grand exercice vu ensemble.

# mercredi 21 décembre
1. Révisions nomenclature vue en classe et en utilisant par exemple [ce site](https://guy-chaumeton.pagesperso-orange.fr/tsnomenc.htm)
2. Sais tu refaire la démonstration sur le titre massique ?
3. __Méthodes indispensables 23__

# jeudi 22 décembre
## Chapitre 3 : Suivi par méthode physique
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP). 
2. S'entraîner sur ce théma [labo_lycée](https://labolycee.org/thema-dosage-par-etalonnage)

# vendredi 23 décembre
## Chapitre 4 : Forces et mouvements 
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. S'entrainer sur les [parties 1 et 2 du sujet de la logan](https://labolycee.org/la-logan-au-banc-dessai)  et ce second sujet du lancement d'un satellite  [Partie 1](https://labolycee.org/lancement-dun-satellite-meteorologique)  
3. Fiches méthodes indispensables __18, 19, 20__

[En musique svp ! on clique....](https://www.youtube.com/watch?v=yXQViqx6GMY&ab_channel=MariahCareyVEVO  )
![](https://acegif.com/wp-content/uploads/2021/4fh5wi/santa-claus-26.gif)  

# lundi 26  décembre
## Chapitre 5 : Mouvement dans un champ uniforme
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. S'entraîner sur [ce sujet](https://labolycee.org/eruption-de-la-montagne-pelee-en-1902)  

# mardi 27 décembre
1. Faire les exercices sur le champ électrique de votre livre
2.  S'entraîner sur [ce sujet](https://labolycee.org/determination-du-rapport-em-pour-lelectron)

# mercredi 28 décembre
## Chapitre 6 : Modélisation macroscopique de la transformation chimique
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. S'entraîner sur [ce sujet](https://labolycee.org/suivi-cinetique-par-conductimetrie) ; attention la définition de la vitesse n'est pas formulée de la même façon dans le nouveau programme mais cela ne change rien.  

# jeudi 29 décembre
## Chapitre 7 : circuits capactifs
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. S'entraîner sur les exerices 32 et 34 p 495 ;
3. __Fiche méthodes indispensables 13__  

# vendredi 30 décembre
## Chapitre 8 : Critère de réaction spontané  
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. Refaire le DS 3 ex 2


# samedi 31 décembre
## Chapitre 9 : Mouvement des satellites et des planètes
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. Refaire les derniers gros exercices du livre et S'entraîner sur [ce sujet autre que la partie 1 déjà faite](https://labolycee.org/lancement-dun-satellite-meteorologique)



![](https://www.gif-maniac.com/gifs/6/6345.gif)  

J'attends vos vidéos de chorégraphies sur [cette musique!](https://www.youtube.com/watch?v=fCZVL_8D048)
 
# dimanche 1 janvier
Appelez ( en vrai , pas un pov' SMS, ou pire un vocal) les amis, la famille pour souhaiter une bonne année 2023 ! 

# lundi 2 janvier
## Chapitre 10 : Analyse d'un système par méthode chimique
1. Relire cours, vérifier la qualité de la  fiche(en INCLUANT LES techniques des TP).
2. S'entraîner sur [ce théma](https://labolycee.org/thema-dosage-par-titrage)  


# BRAVO ! VOUS AVEZ BIEN TRAVAILLE !!!!

![](https://acegif.com/wp-content/uploads/2021/4fh5wi/christmas-lights-acegif-7.gif)
