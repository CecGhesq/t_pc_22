// -------------------------------------------------------------------------
//  Auteur : Samuel JACQUINET & Jocelyn CLEMENT
//  Version : 1.0.4
//  Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution 
//  – Pas d’Utilisation Commerciale – Pas de Modification 4.0 International.
// -------------------------------------------------------------------------

var panel01;
var xA = new Array();
var yA = new Array();
var y1A = new Array();
var x2A = new Array();
var y2A = new Array();
var x3A = new Array();
var y3A = new Array();

var xpA = new Array();
var ypA = new Array();
var y2p1A = new Array();
var x2pA = new Array();
var y2pA = new Array();

var nbPoints = 300;
var pas = 0.2;
var a = 3;
var periode = 5;
var phi,phi2;
var slider,valeur,slider2,valeur2;
var b1,b2,b3;
var dephasage,dephasage2;
var a1;
var a2;
var graph;
var panel2;
var afficher;
var longueur,longueur2,longueur3,longueur4,longueur5;
var checkBox1;
var intercon;
var textic;
var intercon2;
var tracker;
var tracker2;
var tracker3;
var tracker4;

function init() {
	
	stage = frame.stage; // stage nom de la sc�ne 
	stageW = frame.width; // largeur et hauteur de la sc�ne
	stageH = frame.height;
	zog("fonction init lanc�e !!"); // commentaire pour la console
	waiter = new zim.Waiter({container:stage, corner:0}); // indicateur d'attente ()
	
	// telechargement des images
	frame.loadAssets(["images/paysage1.jpg"]);
	
	waiter.show(); // indicateur visible
	
	// losque t�l�chargement termin� on lance la fonction chargementOK
	frame.on("complete", chargementOK);
};

function chargementOK(){
	waiter.hide(); // cache l'indicateur	
		Img_fond = frame.asset("images/paysage1.jpg");// def image
		Img_fond.width = stageW;
		Img_fond.height = stageH;
		Img_fond.centerReg(stage); // image centr�e et affich�e
	
	b1 = true;
	b2 = false;
	b3 = false;
	
	
	// ------------------------- Blocs --------------------------------
	
	var panel = new zim.Container(frame.width,50);
	panel.center(stage);
	panel.x = 0;
	panel.y = 0;
	panel.addChild(new zim.Rectangle(panel.width, panel.height, "#444"));
    
	var titre = new zim.Label({
		text: "Interférences de deux sources ponctuelles",
			size: 32,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
    
    titre.center(panel);
	
	panel2 = new zim.Container(stageW-60-535, 585);
	panel2.center(stage);
	panel2.x = 20;
	panel2.y = 70;
	panel2.addChild(new zim.Rectangle(panel2.width, panel2.height, "#FFF"));
	
	var txt_chk1 = new zim.Label({
		text:"Afficher le décalage temporel pour les interférences constructives / destructives",
			size: 15,
			font:"arial",
			color:"Black",
			//fontOptions:"bold"
		});
       
	// case à cocher
	checkBox1 = new zim.CheckBox(20, txt_chk1);
	checkBox1.center(panel2);
    checkBox1.x = 70;
    checkBox1.y = 560;
	
	checkBox1.checked = false;
	afficher = false;
	
	checkBox1.enabled = false;
	
	checkBox1.on("change", function() {
		zog(checkBox1.checked);
		if (checkBox1.checked){
			afficher = false;
		}else {
			afficher = true;
			afficherdt();
		}
	});

	
	var s1 = new zim.Label({
				text:"Source 1: ...",
				size:20,
				font:"arial",
				color:"#0000FF",
				fontOptions:"bold"
    });
	
    var s2 = new zim.Label({
				text:"Source 2: ...",
				size:20,
				font:"arial",
				color:"#FF0000",
				fontOptions:"bold"
    });
    
    var s3 = new zim.Label({
				text:"Source 1 + 2: ...",
				size:20,
				font:"arial",
				color:"#006400",
				fontOptions:"bold"
    });
	
	s1.center(panel2);
	s2.center(panel2);
	s3.center(panel2);
	
	s1.x = 180;s1.y = 65;
	s2.x = 330;s2.y = 65;
	s3.x = 480;s3.y = 65;
	
	var panel3 = new zim.Container(panel2.width,50);
	panel3.center(panel2);
	panel3.x = 0;
	panel3.y = 0;
	panel3.addChild(new zim.Rectangle(panel3.width, panel3.height, "#444"));
	
	var titre2 = new zim.Label({
		text: "Graphiques",
			size: 32,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
    
    titre2.center(panel3);
	
    var panel4 = new zim.Container(stageW-40, 80);
	panel4.center(stage);
	panel4.x = 20;
	panel4.y = 670;
	panel4.addChild(new zim.Rectangle(panel4.width, panel4.height, "#444"));
	
	var txt_bt1 = new zim.Label({
		text:"Source 1",
			size: 25,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
    
    var button1 = new zim.Button(150, 40, txt_bt1,null,null,null,null,15); 
	
	button1.center(panel4);
	button1.x = 30;
	button1.y = 20;
	
	button1.on("click",function(){
		
		if(b1 == true)
		{
			b1 = false;
			button1.color = "#CC6600";
			
			graph.clear();
			
			if(b2 == true && b3 == true)
			{
				graph.plot(x2A,y2A,'#ff0000',false,true);
				graph.plot(x3A,y3A,'#006300',false,true);
			}
			else
			{
				if(b2 == true && b3 == false)
				{
					graph.plot(x2A,y2A,'#ff0000',false,true);
				}
				else
				{
					graph.plot(x3A,y3A,'#006300',false,true);
				}
			}
			
			
			checkBox1.enabled = false;
			
		}
		else
		{
			b1 = true;
			button1.color = "red";
			
			calcul();
			graph.plot(xA,yA,'#0000ff',false,true);
			
			if(b1 == true && b2 == true)
			{
				checkBox1.enabled = true;
			}
			
		}
		
	});
	
	var txt_bt2 = new zim.Label({
		text:"Source 2",
			size: 25,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
    
    var button2 = new zim.Button(150, 40, txt_bt2,null,null,null,null,15); 
	
	button2.center(panel4);
	button2.x = 240;
	button2.y = 20;
	
	button2.on("click",function(){
		
		if(b2 == true)
		{
			b2 = false;
			button2.color = "#CC6600";
			
			graph.clear();
			
			if(b1 == true && b3 == false)
			{
				graph.plot(xA,yA,'#0000FF',false,true);
			}
			else
			{
				if(b1 == false && b3 == true)
				{
					graph.plot(x3A,y3A,'#006300',false,true);
				}
				else
				{
					graph.plot(xA,yA,'#0000FF',false,true);
					graph.plot(x3A,y3A,'#006300',false,true);
				}
			}
			
			checkBox1.enabled = false;
			
		}
		else
		{
			b2 = true;
			button2.color = "red";
			
			calcul2();
			graph.plot(x2A,y2A,'#ff0000',false,true);
			
			if(b1 == true && b2 == true)
			{
				checkBox1.enabled = true;
			}
		}
		
	});
	
	var txt_bt3 = new zim.Label({
		text:"Source 1 + Source 2",
			size: 25,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
    
    var button3 = new zim.Button(280, 40, txt_bt3,null,null,null,null,15); 
	
	button3.center(panel4);
	button3.x = 470;
	button3.y = 20;
	
	button3.on("click",function(){
		
		if(b3 == true)
		{
			b3 = false;
			button3.color = "#CC6600";
			
			graph.clear();
			
			if(b1 == true && b2 == false)
			{
				graph.plot(xA,yA,'#0000ff',false,true);
			}
			else
			{
				if(b1 == false && b2 == true)
				{
					graph.plot(x2A,y2A,'#FF0000',false,true);
				}
				else
				{
					graph.plot(xA,yA,'#0000FF',false,true);
					graph.plot(x2A,y2A,'#FF0000',false,true);
				}
			}
			
		}
		else
		{
			b3 = true;
			button3.color = "red";
			
			calcul3();
			graph.plot(x3A,y3A,'#006400',false,true);
			zog(yA);
			zog(y2A);
			zog(y3A);
			
		}
		
	});
	
	dephasage = new zim.Label({
		text:"Déphasage : 0.00 rad ",
			size: 30,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
		
	dephasage.center(panel4);
	dephasage.x = 790;
	dephasage.y = 25;
	
	dephasage2 = new zim.Label({
		text:"(      )",
			size: 26,
			font:"arial",
			color:"White",
			fontOptions:"bold"
		});
		
	dephasage2.center(panel4);
	dephasage2.x = 1165;
	dephasage2.y = 28;
		
	var panel5 = new zim.Container(535, 250);
	panel5.center(stage);
	panel5.x = 770;
	panel5.y = 70;
	panel5.addChild(new zim.Rectangle(panel5.width, panel5.height, "#FFF"));
	
	var panel6 = new zim.Container(panel5.width,50);
	panel6.center(panel5);
	panel6.x = 0;
	panel6.y = 0;
	panel6.addChild(new zim.Rectangle(panel6.width, panel6.height, "#444"));

	var phase = new zim.Label({
       text:"Phases",
       size:32,
       color:"#FFF",
       fontOptions:"bold"
    });
	
	phase.center(panel6);
	
	var label0 = new zim.Label({
       text:"-",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button0 = new zim.Button({
       label:label0,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
	
	
	/* var rect = new zim.Rectangle({width:40, height:40, color:"#444"});
	rect.center(panel5);
	rect.x = 310;
	rect.y = 90; */
	
	button0.center(panel5);
	button0.x = 310;
	button0.y = 90;
	
	
	
	
	button0.on("mousedown", function() {
		tracker = setInterval(function(){ // zim interval
		
			
			if( phi > 0 && phi <= 69*Math.PI*periode )
			{
				phi -= (Math.PI*periode/8);
				if(String(parseFloat(phi*Math.PI/180).toFixed(2))=="-0.00")
				{
					phi = 0.00;
				}
				valeur.text = "(" + parseFloat(String(phi*Math.PI/180)).toFixed(2) + " rad)";
				slider.currentValue -= (Math.PI*periode/8);
				
				
				if(b1 == false && b2 == true && b3 == false)
				{
					zog("Ne rien faire");
					
				}	
				else
				{
					if(b1 == true && b2 == false && b3  == false)
					{
						graph.clear();
						calcul();
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3  == false)
						{
							graph.clear();
							calcul();
							
							graph.plot(x2A,y2A,'#ff0000',false,true);
			
							graph.plot(xA,yA,'#0000ff',false,true);
						}
						else
						{
							if(b1 == true && b2 == true && b3 == true )
							{
								graph.clear();
								calcul();
								
								calcul3();
								graph.plot(xA,yA,'#0000ff',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
								graph.plot(x3A,y3A,'#006400',false,true);
								
							}
							else
							{
								if(b1 == true && b2 == false && b3 == true )
								{
									graph.clear();
									calcul();
									graph.plot(xA,yA,'#0000ff',false,true);
									calcul3();
									graph.plot(x3A,y3A,'#006400',false,true);
								}
								else
								{
									graph.clear();
									calcul();
									graph.plot(x2A,y2A,'#ff0000',false,true);
									calcul3();
									graph.plot(x3A,y3A,'#006400',false,true);
								}
							}
						}
					}
				}
				
				dephasage.text = "Déphasage : " + parseFloat(String((phi2-phi)*Math.PI/180)).toFixed(2) + " rad";
				dep();
				
				if(intercon != 200 || intercon2 != 200)
				{
					clearInterval(tracker);
				} 
				
			}
		
		},80);
	});
	
	button0.on("pressup", function(){
		//zog(tracker.count);
		clearInterval(tracker);
	});
	
	
	a1 = 3;
	a2 = 3;
	
	var label02 = new zim.Label({
       text:"+",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button02 = new zim.Button({
       label:label02,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
    
	button02.on("mousedown",function(){
		
		tracker2 = setInterval(function(){ // zim interval
		
		if( phi >= 0 && phi < 69*Math.PI*periode )
		{
			phi += (Math.PI*periode/8);
			valeur.text = "(" + parseFloat(String(phi*Math.PI/180)).toFixed(2) + " rad)";
			
			
			slider.currentValue += (Math.PI*periode/8);
			
			if(b1 == false && b2 == true && b3 == false)
			{
				zog("Ne rien faire");
				
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.clear();
					calcul();
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul();
						
						graph.plot(x2A,y2A,'#ff0000',false,true);
						
								
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							calcul();
							
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == true && b2 == false && b3 == true )
							{
								graph.clear();
								calcul();
								graph.plot(xA,yA,'#0000ff',false,true);
								calcul3();
								graph.plot(x3A,y3A,'#006400',false,true);
							}
							else
							{
								graph.clear();
								calcul();
								graph.plot(x2A,y2A,'#ff0000',false,true);
								calcul3();
								graph.plot(x3A,y3A,'#006400',false,true);
							}
						}
					}
				}
			}
			
			dephasage.text = "Déphasage : " + parseFloat(String((phi2-phi)*Math.PI/180)).toFixed(2) + " rad";
			dep();
			
			if(intercon != 200 || intercon2 != 200)
			{
				clearInterval(tracker2);
			} 
			
		}
		
		},80);
	});
	
    button02.center(panel5);
    button02.x = 365;
    button02.y = 90;
	
	button02.on("pressup", function(){
		//zog(tracker.count);
		clearInterval(tracker2);
	});
	
	phi = 0;
	
	valeur = new zim.Label({
		text: "(" + String(0) + " rad)",
			size: 22,
			font:"arial",
			color:"#000",
			fontOptions:"bold"
		});
    
    valeur.center(panel5);
    valeur.x = 420;
    valeur.y = 98;
	
	slider = new zim.Slider({step:(Math.PI*periode/8),vertical:false,barLength:135,barWidth:15,min:0,max:69*Math.PI*periode,useTicks:false});
    slider.center(panel5);
    slider.x = 150;
    slider.y = 105;
	
	choix = 1;
	
	slider.currentValue = phi;
	
	slider.on("change",function(){
		
		phi = slider.currentValue;
		zog(slider.currentValue);
		valeur.text = "(" + parseFloat(String(phi*Math.PI/180)).toFixed(2) + " rad)";
		
		
		
		if(b1 == false && b2 == true && b3 == false)
		{
			zog("Ne rien faire");
			
		}	
		else
		{
			if(b1 == true && b2 == false && b3  == false)
			{
				graph.clear();
				calcul();
				graph.plot(xA,yA,'#0000ff',false,true);
			}
			else
			{
				if(b1 == true && b2 == true && b3  == false)
				{
					graph.clear();
					calcul();

					graph.plot(x2A,y2A,'#ff0000',false,true);
					
							
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3 == true )
					{
						graph.clear();
						calcul();
						
						calcul3();
						graph.plot(xA,yA,'#0000ff',false,true);
						graph.plot(x2A,y2A,'#ff0000',false,true);
						graph.plot(x3A,y3A,'#006400',false,true);
						
					}
					else
					{
						if(b1 == true && b2 == false && b3 == true )
						{
							graph.clear();
							calcul();
							graph.plot(xA,yA,'#0000ff',false,true);
							calcul3();
							graph.plot(x3A,y3A,'#006400',false,true);
						}
						else
						{
							graph.clear();
							calcul();
							graph.plot(x2A,y2A,'#ff0000',false,true);
							calcul3();
							graph.plot(x3A,y3A,'#006400',false,true);
						}
					}
				}
			}
		}
		
		dephasage.text = "Déphasage : " + parseFloat(String((phi2-phi)*Math.PI/180)).toFixed(2) + " rad";
		dep();
		
	});
	
	var titre = new zim.Label({
		text: "Source 1:",
			size: 24,
			font:"arial",
			color:"Black",
			fontOptions:"bold"
		});
    
    titre.center(panel5);
	titre.x = 13;
	titre.y = 98;
	
	
	var titre2 = new zim.Label({
		text: "Source 2:",
			size: 24,
			font:"arial",
			color:"Black",
			fontOptions:"bold"
		});
    
    titre2.center(panel5);
	titre2.x = 10;
	titre2.y = 173;
	
	var label00 = new zim.Label({
       text:"-",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button00 = new zim.Button({
       label:label00,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
	
	button00.center(panel5);
	button00.x = 310;
	button00.y = 165;
	
	button00.on("mousedown",function(){
		
		tracker3 = setInterval(function(){ // zim interval
		
		
		if( phi2 > 0 && phi2 <= 19*180/Math.PI )
		{
			phi2 -= (Math.PI*periode/8);
			if(String(parseFloat(phi2).toFixed(2))=="-0.00")
			{
				phi2 = 0.00;
			}
			valeur2.text = "(" + parseFloat(String(phi2*Math.PI/180)).toFixed(2) + " rad)";
			slider2.currentValue -= (Math.PI*periode/8);
			
			
			if(b1 == false && b2 == true && b3 == false)
			{
				graph.clear();
				calcul2();
				graph.plot(x2A,y2A,'#0000ff',false,true);
				
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					zog("Ne rien faire")
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul2();
						graph.plot(x2A,y2A,'#ff0000',false,true);
						
								
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul2();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == false && b2 == true && b3 == true )
							{
								graph.clear();
								
								calcul2();
								calcul3();
								graph.plot(x2A,y2A,'#ff0000',false,true);
								graph.plot(x3A,y3A,'#006400',false,true);
							}
							else
							{
								graph.clear();
								
								calcul2();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(xA,yA,'#0000ff',false,true);
							}
						}
					}
				}
			}
			
			dephasage.text = "Déphasage : " + parseFloat(String((phi2-phi)*Math.PI/180)).toFixed(2) + " rad";
			dep();
			
			if(intercon != 200 || intercon2 != 200)
			{
				clearInterval(tracker3);
			} 
			
		}
		
		},80);
		
	});
	
	button00.on("pressup", function(){
		//zog(tracker.count);
		clearInterval(tracker3);
	});
	
	var label002 = new zim.Label({
       text:"+",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button002 = new zim.Button({
       label:label002,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
    
    button002.center(panel5);
    button002.x = 365;
    button002.y = 165;
	
	button002.on("mousedown",function(){
		
		tracker4 = setInterval(function(){ // zim interval
		
		if( phi2 >= 0 && phi2 < 19*180/Math.PI )
		{
			phi2 += (Math.PI*periode/8);
			valeur2.text = "(" + parseFloat(String(phi2*Math.PI/180)).toFixed(2) + " rad)";
			slider2.currentValue += (Math.PI*periode/8);
			
			
			if(b1 == false && b2 == true && b3 == false)
			{
				graph.clear();
				calcul2();
				graph.plot(x2A,y2A,'#0000ff',false,true);
				
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					zog("Ne rien faire")
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul2();
						graph.plot(x2A,y2A,'#ff0000',false,true);
						
								
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul2();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == false && b2 == true && b3 == true )
							{
								graph.clear();
								
								calcul2();
								calcul3();
								graph.plot(x2A,y2A,'#ff0000',false,true);
								graph.plot(x3A,y3A,'#006400',false,true);
							}
							else
							{
								graph.clear();
								
								calcul2();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(xA,yA,'#0000ff',false,true);
							}
						}
					}
				}
			}
			
			dephasage.text = "Déphasage : " + parseFloat(String((phi2-phi)*Math.PI/180)).toFixed(2) + " rad";
			dep();
			
			if(intercon != 200 || intercon2 != 200)
			{
				clearInterval(tracker4);
			} 
			
		}
		
		},80);
		
	});
	
	button002.on("pressup", function(){
		//zog(tracker.count);
		clearInterval(tracker4);
	});
	
	phi2 = 0;
	
	valeur2 = new zim.Label({
		text: "(0 rad)",
			size: 22,
			font:"arial",
			color:"#000",
			fontOptions:"bold"
		});
    
    valeur2.center(panel5);
    valeur2.x = 420;
    valeur2.y = 170;
	
	b1 = true;
	b2 = false;
	b3 = false;
	
	slider2 = new zim.Slider({step:(Math.PI*periode/8),vertical:false,barLength:135,barWidth:15,min:0,max:69*Math.PI*periode,useTicks:false});
    slider2.center(panel5);
    slider2.x = 150;
    slider2.y = 180;
	
	slider2.currentValue = phi2;
	
	slider2.on("change",function(){
		
		phi2 = slider2.currentValue;
		zog(slider2.currentValue);
		valeur2.text = "(" + parseFloat(String(phi2*Math.PI/180)).toFixed(2) + " rad)";
		
		if(b1 == false && b2 == true && b3 == false)
		{
			graph.clear();
			calcul2();
			graph.plot(x2A,y2A,'#0000ff',false,true);
			
		}	
		else
		{
			if(b1 == true && b2 == false && b3  == false)
			{
				zog("Ne rien faire")
			}
			else
			{
				if(b1 == true && b2 == true && b3  == false)
				{
					graph.clear();
					calcul2();
					graph.plot(x2A,y2A,'#ff0000',false,true);
					
							
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3 == true )
					{
						graph.clear();
						
						calcul2();
						calcul3();
						graph.plot(xA,yA,'#0000ff',false,true);
						graph.plot(x2A,y2A,'#ff0000',false,true);
						graph.plot(x3A,y3A,'#006400',false,true);
						
					}
					else
					{
						if(b1 == false && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul2();
							calcul3();
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
						}
						else
						{
							graph.clear();
							
							calcul2();
							calcul3();
							
							graph.plot(x3A,y3A,'#006400',false,true);
							graph.plot(xA,yA,'#0000ff',false,true);
						}
					}
				}
			}
		}
		
			
		dephasage.text = "Déphasage : " + parseFloat(String((phi2-phi)*Math.PI/180)).toFixed(2) + " rad";
		dep();
			
		
		
	});
	
	
	
	var panel7 = new zim.Container(535, 305);
	panel7.center(stage);
	panel7.x = 770;
	panel7.y = 350;
	panel7.addChild(new zim.Rectangle(panel7.width, panel7.height, "#FFF"));
	
	var panel8 = new zim.Container(panel7.width,50);
	panel8.center(panel7);
	panel8.x = 0;
	panel8.y = 0;
	panel8.addChild(new zim.Rectangle(panel6.width, panel6.height, "#444"));
	
	var amplitude = new zim.Label({
       text:"Amplitudes",
       size:32,
       color:"#FFF",
       fontOptions:"bold"
    });
	
	amplitude.center(panel8);
	
	var label20 = new zim.Label({
       text:"-",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button20 = new zim.Button({
       label:label20,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
	
	button20.center(panel7);
	button20.x = 345;
	button20.y = 90;
	
	button20.on("click",function(){
		
		
		if( a1 > 0 && a1 <= 8 )
		{
			a1 -= 1;
			valeur3.text = "( " + parseFloat(a1).toFixed(0) + " )";
			slider3.currentValue = a1;
			setupgraph();
			
			if(b1 == false && b2 == true && b3 == false)
			{
				graph.plot(x2A,y2A,'#ff0000',false,true);	
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.clear();
					calcul();
							
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul();
						graph.plot(x2A,y2A,'#ff0000',false,true);		
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == false && b2 == true && b3 == true )
							{
								graph.clear();
								
								calcul();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
							}
							else
							{
								graph.clear();
								
								calcul();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(xA,yA,'#0000ff',false,true);
							}
						}
					}
				}
			}
			
		}
		
	});
	
	var label202 = new zim.Label({
       text:"+",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button202 = new zim.Button({
       label:label202,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
    
    button202.center(panel7);
    button202.x = 400;
    button202.y = 90;
	
	button202.on("click",function(){
		
		if( a1 >= 0 && a1 < 8 )
		{
			a1 += 1;
			valeur3.text = "( " + parseFloat(a1).toFixed(0) + " )";
			slider3.currentValue = a1;
			setupgraph();
			
			if(b1 == false && b2 == true && b3 == false)
			{
				graph.plot(x2A,y2A,'#ff0000',false,true);	
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.clear();
					calcul();
							
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul();
						graph.plot(x2A,y2A,'#ff0000',false,true);		
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == false && b2 == true && b3 == true )
							{
								graph.clear();
								
								calcul();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
							}
							else
							{
								graph.clear();
								
								calcul();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(xA,yA,'#0000ff',false,true);
							}
						}
					}
				}
			}
		}
		
	});
	
	var valeur3 = new zim.Label({
		text: "( " + 3 + " )",
			size: 26,
			font:"arial",
			color:"#000",
			fontOptions:"bold"
		});
    
    valeur3.center(panel7);
	valeur3.x = 460;
    valeur3.y = 95;
	
	var slider3 = new zim.Slider({step:1,vertical:false,barLength:142,barWidth:15,min:0,max:8,useTicks:false});
    slider3.center(panel7);
    slider3.x = 180;
    slider3.y = 105;
	
	slider3.currentValue = 3;
	
	slider3.on("change",function(){
		
		valeur3.text = "( " + parseFloat(slider3.currentValue).toFixed(0) + " )";
		a1 = slider3.currentValue;
		setupgraph();
		
		if(b1 == false && b2 == true && b3 == false)
			{
				graph.plot(x2A,y2A,'#ff0000',false,true);	
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.clear();
					calcul();
							
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul();
						graph.plot(x2A,y2A,'#ff0000',false,true);		
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == false && b2 == true && b3 == true )
							{
								graph.clear();
								
								calcul();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
							}
							else
							{
								graph.clear();
								
								calcul();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(xA,yA,'#0000ff',false,true);
							}
						}
					}
				}
			}
		
	});
	
	var titre3 = new zim.Label({
		text: "Source 1:",
			size: 32,
			font:"arial",
			color:"Black",
			fontOptions:"bold"
		});
    
    titre3.center(panel7);
	titre3.x = 10;
	titre3.y = 95;
	
	
	var titre4 = new zim.Label({
		text: "Source 2:",
			size: 32,
			font:"arial",
			color:"Black",
			fontOptions:"bold"
		});
    
    titre4.center(panel7);
	titre4.x = 10;
	titre4.y = 190;
	
	var label200 = new zim.Label({
       text:"-",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button200 = new zim.Button({
       label:label200,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
	
	button200.center(panel7);
	button200.x = 345;
	button200.y = 185;
	
	button200.on("click",function(){
		
		if( a2 > 0 && a2 <= 8 )
		{
			a2 -= 1;
			valeur4.text = "( " + parseFloat(a2).toFixed(0) + " )";
			slider4.currentValue = a2;
			setupgraph();
			
			if(b1 == false && b2 == true && b3 == false)
			{
				graph.clear();
				calcul2();
				graph.plot(x2A,y2A,'#ff0000',false,true);	
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul2();
						graph.plot(x2A,y2A,'#ff0000',false,true);		
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul2();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == true && b2 == false && b3 == true)
							{
								calcul2();
								calcul3();
								graph.plot(xA,yA,'#0000ff',false,true);
								
								graph.plot(x3A,y3A,'#006400',false,true);
							}
							else
							{
								graph.clear();
								
								calcul2();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
							}
						}
					}
				}
			}
		}
		
	});
	
	var label2002 = new zim.Label({
       text:"+",
       size:32,
       color:"Black",
       fontOptions:"bold"
    });
    
    var button2002 = new zim.Button({
       label:label2002,
       width:40,
       height:40,
       color:"White",
       borderWidth:1,
       borderColor:"#444",
       gradient:.3,
       corner:0
    });
    
    button2002.center(panel7);
    button2002.x = 400;
    button2002.y = 185;
	
	button2002.on("click",function(){
		
		if( a2 >= 0 && a2 < 8 )
		{
			a2 += 1;
			valeur4.text = "( " + parseFloat(a2).toFixed(0) + " )";
			slider4.currentValue = a2;
			setupgraph();
			
			if(b1 == false && b2 == true && b3 == false)
			{
				graph.clear();
				calcul2();
				graph.plot(x2A,y2A,'#ff0000',false,true);	
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul2();
						graph.plot(x2A,y2A,'#ff0000',false,true);		
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul2();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == true && b2 == false && b3 == true)
							{
								calcul2();
								calcul3();
								graph.plot(xA,yA,'#0000ff',false,true);
								
								graph.plot(x3A,y3A,'#006400',false,true);
							}
							else
							{
								graph.clear();
								
								calcul2();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
							}
						}
					}
				}
			}
			
		}
		
	});
	
	var valeur4 = new zim.Label({
		text: "( " + 3 + " )",
			size: 26,
			font:"arial",
			color:"#000",
			fontOptions:"bold"
		});
    
    valeur4.center(panel7);
    valeur4.x = 460;
    valeur4.y = 190;
	
	var slider4 = new zim.Slider({step:1,vertical:false,barLength:142,barWidth:15,min:0,max:8,useTicks:false});
    slider4.center(panel7);
    slider4.x = 180;
    slider4.y = 200;
	
	slider4.currentValue = 3;
	
	slider4.on("change",function(){
		
		valeur4.text = "( " + parseFloat(slider4.currentValue).toFixed(0) + " )";
		a2 = slider4.currentValue;
		setupgraph();
		
		if(b1 == false && b2 == true && b3 == false)
			{
				graph.clear();
				calcul2();
				graph.plot(x2A,y2A,'#ff0000',false,true);	
			}	
			else
			{
				if(b1 == true && b2 == false && b3  == false)
				{
					graph.plot(xA,yA,'#0000ff',false,true);
				}
				else
				{
					if(b1 == true && b2 == true && b3  == false)
					{
						graph.clear();
						calcul2();
						graph.plot(x2A,y2A,'#ff0000',false,true);		
						graph.plot(xA,yA,'#0000ff',false,true);
					}
					else
					{
						if(b1 == true && b2 == true && b3 == true )
						{
							graph.clear();
							
							calcul2();
							calcul3();
							graph.plot(xA,yA,'#0000ff',false,true);
							graph.plot(x2A,y2A,'#ff0000',false,true);
							graph.plot(x3A,y3A,'#006400',false,true);
							
						}
						else
						{
							if(b1 == true && b2 == false && b3 == true)
							{
								calcul2();
								calcul3();
								graph.plot(xA,yA,'#0000ff',false,true);
								
								graph.plot(x3A,y3A,'#006400',false,true);
							}
							else
							{
								graph.clear();
								
								calcul2();
								calcul3();
								
								graph.plot(x3A,y3A,'#006400',false,true);
								graph.plot(x2A,y2A,'#ff0000',false,true);
							}
						}
					}
				}
			}
		
	});
	
	// -------------------------- Tracé du graphique --------------------
	
	
	graph = new Graph_zim(panel2,0,20,-6,6,45,320,640,450,"black");	//Dessine le graphe		
	
	graph.drawgrid(5,1,1,0.2);
	graph.drawaxes("t(s)","y(cm)","black");
	
	graph.cacheGrille(false);
	
	calcul();
	zog(xA);
	zog(yA);
	
	graph.plot(xA,yA,'#0000ff',false,true);
	
	button1.color = "Red";
	
	intercon = 200;
	
	   
	var lab = new zim.Label({
        text:"Conception : CLEMENT J & JACQUINET S Réalisation : JACQUINET S",
        size: 11,
        font:"arial",
        color:"White",
        fontOptions:"bold"
    });

    lab.center(stage);
    lab.x = 6;
    lab.y = 650;
    
    lab.rotation = -90; 
	
		
}

function calcul(){
	var t = 0;
	for (var i=0; i<=100; i++){
		//xA[i] = (i-50)*0.08;
		xA[i] = t;				
		//xA[i] = (i-50)*0.05;	
		yA[i] = f(xA[i]);						 				
		//y1A[i] = f(-xA[i]);
		t = t + 0.2;
	}
}

function calculp(){
	var t = 0;
	for (var i=0; i<=300; i++){
		//xA[i] = (i-50)*0.08;
		xpA[i] = t;				
		//xA[i] = (i-50)*0.05;	
		ypA[i] = f(xpA[i]);						 				
		//y1A[i] = f(-xA[i]);
		t = t + 0.1;
	}
}

function f(x){
	var y;
	y = a1*(Math.sin((2*Math.PI/periode)*x+(phi*Math.PI/180)));			

	return y;
}

function calcul2(){
	var t = 0;
	for (var i=0; i<=100; i++){
		//xA[i] = (i-50)*0.08;
		x2A[i] = t;				
		//xA[i] = (i-50)*0.05;	
		y2A[i] = f2(x2A[i]);						 				
		//y1A[i] = f(-xA[i]);
		t = t + 0.2;
	}
}

function calcul2p(){
	var t = 0;
	for (var i=0; i<=300; i++){
		//xA[i] = (i-50)*0.08;
		x2pA[i] = t;				
		//xA[i] = (i-50)*0.05;	
		y2pA[i] = f2(x2pA[i]);						 				
		//y1A[i] = f(-xA[i]);
		t = t + 0.1;
	}
}

function f2(x){
	var y;
	y = a2*(Math.sin((2*Math.PI/periode)*x+(phi2*Math.PI/180)));			

	return y;
}

function calcul3(){
	var t = 0;
	calculp();
	calcul2p();
	
	for (var i=0; i<=200; i++){
		
		x3A[i] = t;				
			
		y3A[i] = ypA[i]+y2pA[i];						 				
		
		t = t + 0.1;
	}
}


function dep(){
	
	try
	{
		panel2.removeChild(longueur);
		panel2.removeChild(longueur2);
		panel2.removeChild(longueur3);
		panel2.removeChild(longueur4);
		panel2.removeChild(longueur5);
	}
	catch(error)
	{
		zog("erreur");
	}
	
	try
	{
		panel2.removeChild(textic);
	}
	catch(error)
	{
		zog("erreur");
	}
	
	var dp = (parseFloat(phi2).toFixed(2) - parseFloat(phi).toFixed(2))*Math.PI/180;
	zog("dp : " + dp);
	var d = parseFloat(dp/(6.305580589584867)).toFixed(2);
	zog("d :" + d);
	
	if(is_int(d) == true)
	{
		if(d == 1)
		{
			dephasage2.text = "(T)";
			intercon2 = 1;
		}
		else
		{
			if(d == -1)
			{
				dephasage2.text = "(-T)";
				intercon2 = -1;
			}
			else
			{
				dephasage2.text = "(" + parseInt(dp/(6.237)) + "T)";
				intercon2 = parseInt(dp/(6.237));
			}
		}
		
		if(checkBox1.checked == true)
		{
			afficherdt2();
		}
			
	}
	else
	{
		var dpp = String(parseFloat(dp).toFixed(2));
		zog("dpp: " + dpp);
		
		switch(dpp)
		{
			case "3.15":
			dephasage2.text = "(T/2)";
			intercon = 1;
			break;
			case "9.42":
			dephasage2.text = "(3T/2)";
			intercon = 3;
			break;
			case "15.70":
			dephasage2.text = "(5T/2)";
			intercon = 5;
			break;
			case "-3.15":
			dephasage2.text = "(-T/2)";
			intercon = -1;
			break;
			case "-9.42":
			dephasage2.text = "(-3T/2)";
			intercon = -3;
			break;
			case "-15.70":
			dephasage2.text = "(-5T/2)";
			intercon = -5;
			break;
			default:
			dephasage2.text = "(   )";
			intercon = 200;
			intercon2 = 200;
			break;
		}
		
		
		
		if(checkBox1.checked == true)
		{
			afficherdt();
		}
		else
		{
			try
			{
				panel2.removeChild(longueur);
				panel2.removeChild(longueur2);
				panel2.removeChild(longueur3);
				panel2.removeChild(longueur4);
				panel2.removeChild(longueur5);
			}
			catch(error)
			{
				zog("erreur");
			}
			
			try
			{
				panel2.removeChild(textic);
			}
			catch(error)
			{
				zog("erreur");
			}
		}
		
	}
	
}

function is_int(value){
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){ 
      
      return true;
  } else {
      
      return false;
  }
}

function setupgraph()
{
	graph.clearall();
	
	var aa = a1+a2;
	
	graph = new Graph_zim(panel2,0,20,-aa,aa,45,320,640,450,"black");	//Dessine le graphe		
	
	graph.drawgrid(5,1,1,0.5);
	graph.drawaxes("t(s)","y(cm)","black");
	
	graph.cacheGrille(false);
	
}

function afficherdt()
{
	
	if(intercon != 200)
	{

		var tt,tt2;
		var po,po2;
		var poo,poo2;
		
		var phase1 = phi*Math.PI/180;
		var phase2 = phi2*Math.PI/180;
		
		if(phase2 - phase1 != 0)
		{
			if(phase2 < phase1)
			{
				
				var co = false;
				var i = 0;
				var avant = 0;
				while(co == false)
				{
					if(avant <= yA[i])
					{
						i ++;
						zog(i);
					}
					else
					{
						poo = 83 + xA[i]*640/20;
						zog(poo);
						co = true;
					}
					
				}
				
				po2 = 2.5*640/20;
				
				poo2 = poo + -1*intercon*po2;
				
			}
			else
			{
				
				var co = false;
				var i = 0;
				var avant = 0;
				while(co == false)
				{
					if(avant <= yA[i])
					{
						i ++;
						zog(i);
					}
					else
					{
						poo = 83 + xA[i]*640/20;
						zog(poo);
						co = true;
					}
					
				}
				
				po2 = 2.5*640/20;
				
				poo2 = poo + intercon*po2;
			}
			
			
			
			textic = new zim.Label({
				text: "",
				size: 20,
				font:"arial",
				color:"Black",
				fontOptions:"bold"
			});
			
			switch(intercon)
			{
				case 1:
				textic.text = "T/2";
				dephasage.text = "Déphasage : π rad";
				break;
				case 3:
				textic.text = "3T/2";
				dephasage.text = "Déphasage : 3π rad";
				break;
				case 5:
				textic.text = "5T/2";
				dephasage.text = "Déphasage : 5π rad";
				break;
				case -1:
				textic.text = "-T/2";
				dephasage.text = "Déphasage : -π rad";
				break;
				case -3:
				textic.text = "-3T/2";
				dephasage.text = "Déphasage : -3π rad";
				break;
				case -5:
				textic.text = "-5T/2";
				dephasage.text = "Déphasage : -5π rad";
				break;
				default:
				textic.text = "";
				break;
			}
			
			textic.center(panel2);
			textic.x = poo-20+(poo2-poo)/2;
			textic.y = 150;
			
			longueur = new zim.Shape();
			var l = longueur.graphics; // shorter reference to graphics object
			l.setStrokeStyle(2).beginStroke("Black").moveTo(poo,180).lineTo(poo2,180);
								
			panel2.addChild(longueur);
			
			longueur2 = new zim.Shape();
			var l2 = longueur2.graphics; // shorter reference to graphics object
			l2.setStrokeStyle(2).beginStroke("Black").moveTo(poo,180).lineTo(poo+10,175);
								
			panel2.addChild(longueur2);
			
			longueur3 = new zim.Shape();
			var l3 = longueur3.graphics; // shorter reference to graphics object
			l3.setStrokeStyle(2).beginStroke("Black").moveTo(poo,180).lineTo(poo+10,185);
								
			panel2.addChild(longueur3);
			
			longueur4 = new zim.Shape();
			var l4 = longueur4.graphics; // shorter reference to graphics object
			l4.setStrokeStyle(2).beginStroke("Black").moveTo(poo2,180).lineTo(poo2-10,175);
								
			panel2.addChild(longueur4);
			
			longueur5 = new zim.Shape();
			var l5 = longueur5.graphics; // shorter reference to graphics object
			l5.setStrokeStyle(2).beginStroke("Black").moveTo(poo2,180).lineTo(poo2-10,185);
								
			panel2.addChild(longueur5);
			
		}
	
	}
	
		
	
	
}



function afficherdt2()
{
	
	

		var tt,tt2;
		var po,po2;
		var poo,poo2;
		
		var phase1 = phi*Math.PI/180;
		var phase2 = phi2*Math.PI/180;
		
		if(phase2 - phase1 != 0)
		{
			if(phase2 < phase1)
			{
				
				var co = false;
				var i = 0;
				var avant = 0;
				while(co == false)
				{
					if(avant <= yA[i])
					{
						i ++;
						zog(i);
					}
					else
					{
						poo = 163 + xA[i]*640/20;
						zog(poo);
						co = true;
					}
					
				}
				
				if(intercon2 == -3)
				{
					poo = poo - 160;
				}
				
				po2 = 5*640/20;
				
				poo2 = poo + -1*intercon2*po2;
				
			}
			else
			{
				
				var co = false;
				var i = 0;
				var avant = 0;
				while(co == false)
				{
					if(avant <= yA[i])
					{
						i ++;
						zog(i);
					}
					else
					{
						poo = 163 + xA[i]*640/20;
						zog(poo);
						co = true;
					}
					
				}
				
				if(intercon2 == 3)
				{
					poo = poo - 160;
				}
				
				po2 = 5*640/20;
				
				poo2 = poo + intercon2*po2;
			}
			
			
			
			textic = new zim.Label({
				text: "",
				size: 20,
				font:"arial",
				color:"Black",
				fontOptions:"bold"
			});
			
			switch(intercon2)
			{
				case 1:
				textic.text = "T";
				dephasage.text = "Déphasage : 2π rad";
				break;
				case 2:
				textic.text = "2T";
				dephasage.text = "Déphasage : 4π rad";
				break;
				case 3:
				textic.text = "3T";
				dephasage.text = "Déphasage : 6π rad";
				break;
				case -1:
				textic.text = "-T";
				dephasage.text = "Déphasage : -2π rad";
				break;
				case -2:
				textic.text = "-2T";
				dephasage.text = "Déphasage : -4π rad";
				break;
				case -3:
				textic.text = "-3T";
				dephasage.text = "Déphasage : -6π rad";
				break;
				default:
				textic.text = "";
				break;
			}
			
			if(intercon2 != 0)
			{
				textic.center(panel2);
				textic.x = poo-10+(poo2-poo)/2;
				textic.y = 150;
				
				longueur = new zim.Shape();
				var l = longueur.graphics; // shorter reference to graphics object
				l.setStrokeStyle(2).beginStroke("Black").moveTo(poo,180).lineTo(poo2,180);
									
				panel2.addChild(longueur);
				
				longueur2 = new zim.Shape();
				var l2 = longueur2.graphics; // shorter reference to graphics object
				l2.setStrokeStyle(2).beginStroke("Black").moveTo(poo,180).lineTo(poo+10,175);
									
				panel2.addChild(longueur2);
				
				longueur3 = new zim.Shape();
				var l3 = longueur3.graphics; // shorter reference to graphics object
				l3.setStrokeStyle(2).beginStroke("Black").moveTo(poo,180).lineTo(poo+10,185);
									
				panel2.addChild(longueur3);
				
				longueur4 = new zim.Shape();
				var l4 = longueur4.graphics; // shorter reference to graphics object
				l4.setStrokeStyle(2).beginStroke("Black").moveTo(poo2,180).lineTo(poo2-10,175);
									
				panel2.addChild(longueur4);
				
				longueur5 = new zim.Shape();
				var l5 = longueur5.graphics; // shorter reference to graphics object
				l5.setStrokeStyle(2).beginStroke("Black").moveTo(poo2,180).lineTo(poo2-10,185);
									
				panel2.addChild(longueur5);
			}
			
		}
	
	
}

function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}