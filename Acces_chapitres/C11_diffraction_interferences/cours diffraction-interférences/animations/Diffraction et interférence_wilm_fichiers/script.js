// ========== GÉNÉRAL ==========
const PI = Math.PI;

fente = "simple";

lambda = 680   // longueur d'onde en nm
document.getElementById("curseurLambda").value = 680;
largeurFente = 200000   // largueur des fentes en nm
distanceFentes = 1000000  // distance entre les fentes en nm
D = 200    // distance entre les fentes et l'écran en cm
ppc = 200  // echelle : pixels par cm

RGB = lambdaToCouleur(lambda);

function interference(posx) {  // x en cm
    switch (fente) {
        case "simple" :
            return 1;
        case "double" : 
            tp = PI*distanceFentes*posx/lambda/D;
            return (Math.cos(tp))**2;
    }
    
}

function diffraction(x) {   // x en cm
    if (x !=0) {
        temp = PI*largeurFente/lambda/D*x;
        return ((Math.sin(temp)/temp)**2);
    } else {
        return 1
    }
    
}

function lambdaToCouleur(lambda) {
    var r;
    var v;
    var b;

    if (lambda<380) {
        r=0;
        v=0;
        b=0;
    } else if (lambda < 440) {
        r = -(lambda - 440) / (440 - 380);
        v=0;
        b=1;
    } else if (lambda <= 490) {
        r=0;
        v = (lambda - 440) / (490 - 440);
        b=1;
    } else if (lambda <= 510) {
        r=0;
        v=1;
        b = -(lambda - 510) / (510 - 490);
    } else if (lambda <= 580) {
        r = (lambda - 510) / (580 - 510);
        v=1;
        b=0;
    } else if (lambda <= 645) {
        r=1;
        v = -(lambda - 645) / (645 - 580);
        b=0;
    } else {
        r=1;
        v=0;
        b=0;
    }

    r=Math.max(0,Math.min(1,r))*255;
    v=Math.max(0,Math.min(1,v))*255;
    b=Math.max(0,Math.min(1,b))*255;

    return {R:r,G:v,B:b};
}


// ========== GRAPHIQUE ==========

canvas1 = document.getElementById("canvas_graphique");
stage1 = new createjs.Stage(canvas1);

// ----- Axes et échelles
axes = new createjs.Shape();
stage1.addChild(axes);
axes.x = 500;
axes.y = 350;

axes.graphics.ss(4,1,1).s("#000");
//axe horizontal
axes.graphics.mt(-498,0).lt(495,0);
axes.graphics.mt(485,-8).lt(495,0).lt(485,8);
//échelle horizontale : petites graduations
axes.graphics.ss(2,1,1).s("#000");
let pos = ppc/10;
while (pos < 500) {
    axes.graphics.mt(pos,0).lt(pos,5);
    axes.graphics.mt(-pos,0).lt(-pos,5);
    pos += ppc/10;
}

//échelle horizontale : grandes graduations
axes.graphics.ss(4,1,1).s("#000");
axes.graphics.mt(-2*ppc,0).lt(-2*ppc,8);
axes.graphics.mt(-ppc,0).lt(-ppc,8);
axes.graphics.mt(ppc,0).lt(ppc,8);
axes.graphics.mt(2*ppc,0).lt(2*ppc,8);
//axe vertical
axes.graphics.mt(0,10).lt(0,-320);
axes.graphics.mt(-8,-310).lt(0,-320).lt(8,-310);

// ----- Courbe -----
courbe = new createjs.Shape();
stage1.addChild(courbe);
courbe.x = 500;
courbe.y = 350;

function traceCourbe() {
    RGB = lambdaToCouleur(lambda);
    couleur = "rgb(" + RGB.R + "," + RGB.G + "," + RGB.B + ")";
    courbe.graphics.clear();
    courbe.graphics.ss(4,1,1).s(couleur);
    courbe.graphics.mt(-498,-300*diffraction(-500/ppc)*interference(500/ppc));
    for (let i = -498; i<=498; i+=2) {
        courbe.graphics.lt(i,-300*diffraction(i/ppc)*interference(i/ppc));
    }
    courbe.graphics.es();
    stage1.update();
}

traceCourbe();


// ========== ÉCRAN ==========

canvas2 = document.getElementById("canvas_ecran");
stage2 = new createjs.Stage(canvas2);

figure = new createjs.Shape();
stage2.addChild(figure);
figure.x = 500;
figure.y = 50;


//figure
function traceFigure() {
    figure.graphics.clear();
    figure.graphics.ss(2,1);
    for (let i = -500; i < 500; i+=2) {
        intensite = diffraction(i/ppc)**0.4 * interference(i/ppc)
        r = RGB.R * intensite;
        g = RGB.G * intensite;
        b = RGB.B * intensite;
        couleur = "rgb("+r+","+g+","+b+")";
        figure.graphics.ls(["#000",couleur, couleur,"#000"], [0,0.45,0.55,1],-0,-30,0,30);
        figure.graphics.mt(i,-30).lt(i,30);
    }
    

    //
    stage2.update();
}
traceFigure();

//échelle
echelle = new createjs.Shape();
stage2.addChild(echelle);
echelle.graphics.ss(2,1,1).s("#fff");
echelle.graphics.mt(10,95).lt(10,100).lt(10+ppc,100).lt(10+ppc,95);

text = new createjs.Text("1 cm", "16px Montserrat", "#fff");
stage2.addChild(text);
text.textAlign = "center"
text.x = 10 + ppc/2;
text.y = 78;

stage2.update();

// ========== Curseurs ==========

// ----- Curseur "Longueur d'onde"
document.getElementById("curseurLambda").addEventListener("input", function() {
    lambda = this.value;
    document.getElementById("affLambda").innerHTML = "" + lambda + " nm"
    traceCourbe();
    traceFigure();
});

// ----- Curseur "Largeur fente"
document.getElementById("curseurLargeurFente").addEventListener("input", function() {
    largeurFente = this.value;
    document.getElementById("affLargeurFente").innerHTML = "" + largeurFente/1000 + " µm"
    traceCourbe();
    traceFigure();
})

// ----- Curseur "Distance"
document.getElementById("curseurDistance").addEventListener("input", function() {
    D = this.value;
    document.getElementById("affDistance").innerHTML = "" + D + " cm"
    traceCourbe();
    traceFigure();
})

// ----- Curseur "Largeur fonte"
document.getElementById("curseurDistanceFentes").addEventListener("input", function() {
    distanceFentes = this.value;
    document.getElementById("affDistanceFentes").innerHTML = "" + distanceFentes/1000 + " µm"
    traceCourbe();
    traceFigure();
})






// ========== GESTION DES ONGLETS ==========

function onClickOnglet1() {
    document.getElementById("onglet1").style.backgroundColor = "#fff";
    document.getElementById("onglet1").style.color = "#000";
    document.getElementById("onglet2").style.backgroundColor = "#ddd";
    document.getElementById("onglet2").style.color = "#666";
    fente = "simple";
    traceCourbe();
    traceFigure();
}

function onClickOnglet2() {
    document.getElementById("onglet1").style.backgroundColor = "#ddd";
    document.getElementById("onglet1").style.color = "#666";
    document.getElementById("onglet2").style.backgroundColor = "#fff";
    document.getElementById("onglet2").style.color = "#000";
    fente = "double";
    traceCourbe();
    traceFigure();
}

document.getElementById("onglet1").addEventListener("click", onClickOnglet1);

document.getElementById("onglet2").addEventListener("click",  onClickOnglet2);

onClickOnglet1();