# Mesure de la tension aux bornes de C d'un circuit RC
# u(t) est un échelon sur Pin P1 (entrée)
# V(t) signal de sortie mesuré sur C sur Pin P2
# R=10K Ohms
# C=100µF
# Tau=RC=1s
# mesures de 150 échantillons

from microbit import *
import utime
# période d'échantillonnage ms
te = 50
# nombre d'échantillons
nb_echantillons = 150
mesures_csv = ""
# signal u(t)=0 au départ
pin0.write_digital(0)
# on attend que le condensateur soit déchargé
while pin2.read_analog() > 4:
    pass
display.show(Image.HAPPY)
t_d = utime.ticks_ms()
for num_echantillon in range(nb_echantillons):
    tension_c = pin2.read_analog()
    mesures_csv = mesures_csv + str(tension_c*3.3/1023) + "\r\n"
    while (utime.ticks_ms() - t_d) <= (te*num_echantillon):
        if num_echantillon == 10:
            # on crée l'échelon de tension sur le circuit
            # après 10 échantillons
            pin0.write_digital(1)
with open('tension_rc.csv', 'w') as my_file:
    my_file.write(mesures_csv)
pin0.write_digital(0)
display.show(Image.YES)