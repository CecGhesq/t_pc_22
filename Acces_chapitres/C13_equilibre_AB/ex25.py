#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ====================================================================
# EXERCICE 25 p173                                     NIVEAU STANDARD
# ====================================================================
"""
Programme permettant de tracer le diagramme de distribution des
espèces du couple acide-base CH3COOH/CH3COO- de pKa = 4,8.
Pour une valeur de pH donnée, la proportion à l'équilibre en quantité 
d'acide vaut rA = 1/[1+10^(pH-pKa)] et celle en quantité de base
vaut rB = 1/[1+10^(pKa-pH)].
"""
import numpy as np                        
from matplotlib import pyplot as plt

X=np.linspace(...A compléter...) # Echantillonnage de l'axe en pH

# Calcul des proportions à l'équilibre
rA=[...A compléter... for pH in X] # en quantité d'acide (en %)
rB=...A compléter...               # en quantité de base (en %)

# Affichage du diagramme de distribution des espèces
plt.plot(X,rA,'r+',label='CH$_3$COOH')   # pour l'acide 
plt.plot(...A compléter...)# pour la base 

# Mise en forme de la fenêtre graphique
plt.suptitle(...A compléter...)  # Titre supérieur
plt.title(...A compléter...)     # Titre
plt.xlabel(...A compléter...)    # Label axe des abscisses 
plt.ylabel(...A compléter...)    # Label axe des ordonnées
plt.axis(...A compléter...) # Minimum et maximum des axes
plt.grid(ls=':')       # Affiche une grille
plt.legend(loc=7)      # Affiche la légende
plt.show()             # Affiche la figure


