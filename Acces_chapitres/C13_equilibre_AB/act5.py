#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ==============================================================================
# ACTIVITE 5 p164                                                       STANDARD
# Programme permettant de tracer le diagramme de distribution des espèces d'un 
# couple acide-base de pKa donné.
# ==============================================================================

import numpy as np                        
from matplotlib import pyplot as plt

# Demande du pKa du couple à l'utilisateur :
pKa=float(input(.....à compléter.....))
# Echantillonnage de l'axe en pH :
X=np.linspace(.....à compléter.....)
# Calcul des proportions à l'équilibre en quantité d'acide (valeurs en %) :
rA=[100/(1+10**(pH-pKa)) for pH in X]
# Calcul des proportions à l'équilibre en quantité de base (valeurs en %) :
rB=[.....à compléter.....]

# Affichage du diagramme de distribution des espèces :
plt.plot(X,rA,'r+',label='espèce acide')       # pour l'acide
plt.plot(.....à compléter.....)                # pour la base

# Mise en forme de la fenêtre graphique :                
plt.title('Diagramme de distribution des espèces')# Titre
plt.xlabel(.....à compléter.....)                 # Label axe des abscisses 
plt.ylabel(.....à compléter.....)                 # Label axe des ordonnées
plt.axis([.....à compléter.....])                 # Minimum et maximum des axes
plt.grid(which='both',linestyle='--')             # Affiche une grille
plt.legend(loc=7)                                 # Affiche la légende
plt.show()                                        # Affiche la figure