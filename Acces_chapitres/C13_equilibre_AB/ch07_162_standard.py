#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ==============================================================================
# ACTIVITE 3 p162                                                       STANDARD
# Programme permettant de déterminer le taux d'avancement final d'une 
# transformation modélisée par la réaction d'un acide sur l'eau, à partir de la 
# connaissance du pKa de l'acide du couple acide-base et de la concentration en
# quantité d'acide apporté c.
# ==============================================================================

# Demande du pKa de l'acide à l'utilisateur :
pKa=float(input(...à compléter...))

# Demande de la concentration en acide apportée c à l'utilisateur :
c=...à compléter...

# Initialisation du taux d'avancement :
tau=...à compléter...
# Affectation de l'intervalle du taux d'avancement :
dtau=...à compléter...
# Calcul de la différence entre Ka=Qr,éqb et Qr
diff=...à compléter...

# Boucle simulant l'avancement progressif de la transformation,
# tant que l'équilibre n'est pas atteint :
while diff>0:
    tau=tau+dtau                      # Progression du taux d'avancement
    diff=10**(-pKa)-c*tau**2/(1-tau)  # Calcul de la nouvelle différence

# Affichage de la valeur du taux d'avancement final, à l'équilibre :
...à compléter...