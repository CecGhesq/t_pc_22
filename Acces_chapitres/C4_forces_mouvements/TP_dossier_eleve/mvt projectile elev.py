#!/usr/bin/env python3
# -*- coding: utf-8
# ================================================================================
# Chapitre 11 : Activité 3                                  niveau standard
# ================================================================================
"""
Programme permettant de représenter les positions successives du centre de
la bille issues d'un pointage vidéo ainsi que les vecteurs vitesse
et les vecteurs accélération à partir d'un fichier de données de format .txt.
"""

"""
Les blocs de lignes de codes entre 2 séries de 3 guillemets sont des commentaires:
ils sont inactifs lors de l'exécution du programme.
"""

"""
Ce code source permet de représenter la trajectoire et les vecteurs vitesse et
accélération du centre de masse de la bille lors du mouvement.

Les lignes de codes allant de 36 à 50 permettent l'extraction des données du
pointage et ne doivent pas être modifiées.

- Exécuter le code source pour visualiser la figure produite.

- Réaliser ensuite les taches suivantes :

a. Calculer les coordonnées et la norme des vecteurs variation de vitesse
et accélération ;

b. Représenter en bleu les vecteurs accélération tous les p points.
"""

import matplotlib.pyplot as plt

# A NE PAS MODIFIER ==============================================================
# Extraction des données du fichier texte de pointage

t,x,y=[],[],[] # définition de 3 listes vides pour les 3 variables t, x et y

# ouverture du fichier texte avec la méthode with
with open("mvtprojectile.txt", "r") as fichier:
    header = [fichier.readline() for i in range(2)]
    # Lecture de la première ligne de DONNEES en remplaçant la virgule par le point
    line = fichier.readline().replace(',','.')
    # Bloc d'instructions à réaliser sur chaque ligne de données
    while line:
        # Séparation des valeurs d'une ligne selon le séparateur indiqué dans split()
        ti,xi,yi=line.split('/t')
        t.append(float(ti)) # dates (en s)
        x.append(float(xi)) # abscisse (en m)
        y.append(float(yi)) # ordonnée (en m)
        line=fichier.readline().replace(',','.') # Lecture de la ligne de données suivante

# ================================================================================

N=len(t) # Nombre de positions
print("\nLe pointage comporte",N,"positions numérotées de",0,"à",N-1,".")

# Tracé des points de la trajectoire y=f(x)
plt.title('Vecteurs vitesse et accélération') # titre
plt.xlabel('x (en m)')                        # légende de l'axe des abscisses
plt.ylabel('y (en m)')                        # légende de l'axe des abscisses
plt.axis('equal')                             # même échelle sur les axes

# Représentation des points de coordonnées (x(t),y(t)),rouge'r',forme'o',taille 2
plt.plot(x,y,'ro',ms=2)

# Calculs des coordonnées et de la norme des vecteurs vitesse
# rangées dans 3 listes vxi, vyi et vi

vx,vy,v=[''],[''],[''] # La première valeur de chaque liste est remplie par
                       # un espace pour la position 0 non calculable

for i in range(1,N-1) :
    # Calcul de la coordonnée vxi du vecteur vitesse sur l'axe x au point n°i
    vxi=(x[i+1]-x[i-1])/(t[i+1]-t[i-1])
    vx.append(vxi) # insère la valeur vxi en dernière position de la liste vx

    # Calcul de la coordonnée vyi du vecteur vitesse sur l'axe y au point n°i
    vyi=(y[i+1]-y[i-1])/(t[i+1]-t[i-1])
    vy.append(vyi) # insère la valeur vyi en dernière position de la liste vy

    # Calcul de la norme vi du vecteur vitesse au point n°i
    vi=(vxi**2+vyi**2)**(1/2)
    v.append(vi)  # insère la valeur vi en dernière position de la liste v

# Option pour ne représenter qu'un vecteur vitesse tous les n points
print("\nReprésenter :")
n=int(input('\n- un vecteur vitesse tous les n points, n= '))

# Représentation des vecteurs vitesse tous les n points
# Affichage des coordonnées vxi, vyi et de la norme vi

print("\nPoint n°"," \t vx (en m.s-1)", " \t vy (en m.s-1)"," \t v (en m.s-1)")

for i in range(1,N-1,n) :
    # Représente au point n°i de coordonnées (x[i],y[i])
    # un vecteur de coordonnées (vx[i]/20,vy[i]/20)
    plt.arrow(x[i], y[i], vx[i]/20, vy[i]/20, width=0.0005,
    length_includes_head=True, head_length=0.0025, head_width=0.0025, color='g')
    # Affiche les coordonnées (vxi,vyi) et la norme vi avec 2 décimales
    print(i," \t\t",'%+.2f'% vx[i]," \t\t",'%+.2f'% vy[i]," \t\t",'%.2f'% v[i])

# Question a =====================================================================

# Calculs des coordonnées des vecteurs variation de vitesse et accélération
# rangées dans 5 listes delta_vxi, axi, delta_vyi, axi et ayi

delta_vx,delta_vy=['',''],['',''] # Les 2 premières valeurs sont remplies par une
ax,ay,a=['',''],['',''],['','']   # espace pour les positions 0 et 1 non calculables

for i in range(2,N-2) :
    # Calcul des coordonnées delta_vxi du vecteur variation de vitesse
    # sur l'axe x au point n°i, et des coordoonées axi du vecteur accélération
    delta_vxi=vx[i+1]-vx[i-1]
    ...à compléter... # calcule la valeur axi à partir de delta_vxi
    ...à compléter... # insère la valeur axi en dernière position de la liste ax

    # Calcul des coordonnées delta_vyi du vecteur variation de vitesse
    # sur l'axe y au point n°i, et des coordoonées ayi du vecteur accélération
    ...à compléter... # calcule la valeur delta_vyi
    ...à compléter... # calcule la valeur ayi à partir de delta_vyi
    ...à compléter... # insère la valeur ayi en dernière position de la liste ay

    # Calcul de la norme du vecteur accélération au point n°i
    ...à compléter... # calcule la norme du vecteur accélération au point n°i
    ...à compléter... # insère la valeur ai en dernière position de la liste a

# Question b =====================================================================

# Option pour ne représenter qu'un vecteur accélération tous les p points
p=int(input('\n- un vecteur accélération tous les p points, p= '))

# Représentation des vecteurs accélération tous les n points
# Affichage des coordonnées axi, ayi et de la norme ai

print("\nPoint n°"," \t ax (en m.s-2)", " \t ay (en m.s-2)"," \t a (en m.s-2)")

for i in range(2,N-2,p) :
    # Représente au point n°i de coordonnées (x[i],y[i])
    # un vecteur de coordonnées (ax[i]/50,ay[i]/50)
    ...à compléter...
    ...à compléter... #
    # Affiche les coordonnées (axi,ayi) et la norme ai avec 2 décimales
    print(i," \t\t",'%+.2f'% ax[i]," \t\t",'%+.2f'% ay[i]," \t\t",'%.2f'% a[i])

plt.show()