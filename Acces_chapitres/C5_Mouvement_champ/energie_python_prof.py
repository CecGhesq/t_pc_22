#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Chapitre 6 TP                              Pour copie d'écran manuel 
# =============================================================================

import matplotlib.pyplot as plt

m = 0.057  # Masse en kg
g =10.4 # Intensité de la pesanteur en N/kg

# =============================================================================
# Etape 1 : Importation des données de pointage
# =============================================================================
t,x,y = [],[],[] # Définitions de listes vides pour t, x et y

with open('relevé_points_mod.txt') as fichier :
    header = [fichier.readline() for i in range(2)]# isole les grandeurs et unités du fichier txt dans header"
    line = fichier.readline().replace(',','.') # lit la première ligne suivant l'en-tête en remplaçant le caractère 'virgule' par un 'point'
    while line:
        data0,data1,data2=line.split('\t') #coupe la ligne à chaque espace : on obtient de la chaine dans les trois datas
        t.append(float(data0)) 
        x.append(float(data1)) 
        y.append(float(data2)) 
        line=fichier.readline().replace(',','.')
        
#===========================================================================
# Type des grandeurs : affichage pour le temps par exemple
#===========================================================================
print("les valeurs de t sont", t, "de type : ", type(t))


# =============================================================================
#  Etape 2 : Calcul des coordonnées et de la norme du vecteur vitesse 
# =============================================================================
N = len(t) # Nombre de positions
vx,vy,v = [],[],[] # Définitions de listes vides pour vx, vy et v

for i in range(N-1) : 
    vxi=(x[i+1]-x[i])/(t[i+1]-t[i]) 
    vyi=(y[i+1]-y[i])/(t[i+1]-t[i]) 
    vi=(vxi**2+vyi**2)**(1/2)       
    vx.append(vxi) 
    vy.append(vyi) 
    v.append(vi)   

# =============================================================================
# Etape 3 : Calcul des grandeurs énergétiques
# =============================================================================
Epp,Ec,Em = [],[],[] # Définitions de listes vides pour Epp, Ec et Em

for i in range(N-1) : # en chaque position i
    Eppi=m*g*y[i] # calcul de l'énergie potentielle de pesanteur ; y[i] car élément de la liste d'indice i      
    Eci=0.5*m*v[i]**2  # calcul de l'énergie cinétique ; v[i] car élément de la liste d'indice i 
    Emi=Eppi+Eci  # calcul de l'énergie mécanique   
    Epp.append(Eppi)  # insère...
    Ec.append(Eci)    # ...les valeurs calculées...
    Em.append(Emi)    # ...en fin de liste.

# =============================================================================
# Etape 4: Représentation graphique
# =============================================================================
del t[-1] # Suppression du dernier élément de la liste t

plt.plot(t,Epp,color='b',marker='+', label = '$E_{pp}$')
plt.plot(t,Ec,color='r',marker='x', label = '$E_{c}$')
plt.plot(t,Em,color='g',marker='o', label = '$E_{m}$')
                    
plt.title("Evolution des énergies au cours du temps")   
plt.xlabel("Temps (en s)")              
plt.ylabel("Energie (en J)")         
plt.legend()
plt.grid() # affichage de la grille
plt.show()
