#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Chapitre 5 TP                              Pour copie d'écran manuel 
# =============================================================================

import matplotlib.pyplot as plt

m = ...........à compléter......  # Masse en kg
g = ...........à compléter......   # Intensité de la pesanteur en N/kg

# =============================================================================
# Etape 1 : Importation des données de pointage
# =============================================================================
t,x,y = [],[],[] # Définitions de listes vides pour t, x et y

with open('.................txt') as fichier :
    header = [fichier.readline() for i in range(2)]
    line = fichier.readline().replace(',','.') 
    while line:
        data0,data1,data2=line.split('\t')
        t.append(float(data0)) 
        x.append(float(data1)) 
        y.append(float(data2)) 
        line=fichier.readline().replace(',','.')

# =============================================================================
#  Etape 2 : Calcul des coordonnées et de la norme du vecteur vitesse 
# =============================================================================
N = len(t) # Nombre de positions
vx,vy,v = [],[],[] # Définitions de listes vides pour vx, vy et v

for i in range(N-1) : 
    vxi=(x[i+1]-x[i])/(t[i+1]-t[i]) 
    vyi=(y[i+1]-y[i])/(t[i+1]-t[i]) 
    vi=(vxi**2+vyi**2)**(1/2)       
    vx.append(vxi) 
    vy.append(vyi) 
    v.append(vi)   

# =============================================================================
# Etape 3 : Calcul des grandeurs énergétiques
# =============================================================================
Epp,Ec,Em = [],[],[] # Définitions de listes vides pour Epp, Ec et Em

for i in range(N-1) : # en chaque position i
    Eppi=................ # calcul de l'énergie potentielle de pesanteur       
    Eci=..................  # calcul de l'énergie cinétique
    Emi=.............  # calcul de l'énergie mécanique   
    Epp.append(Eppi)  # insère...
    Ec.append(Eci)    # ...les valeurs calculées...
    Em.append(Emi)    # ...en fin de liste.

# =============================================================================
# Etape 4: Représentation graphique
# =============================================================================
del t[-1] # Suppression du dernier élément de la liste t

plt.plot(t,Epp,color='b',marker='+', label = '$E_{pp}$')
plt.plot(t,Ec,color='r',marker='x', label = '$E_{c}$')
plt.plot(t,Em,color='g',marker='o', label = '$E_{m}$')
                    
plt.title("Evolution des énergies au cours du temps")   
plt.xlabel("Temps (en s)")              
plt.ylabel("Energie (en J)")         
plt.legend()
plt.grid()
plt.show()
