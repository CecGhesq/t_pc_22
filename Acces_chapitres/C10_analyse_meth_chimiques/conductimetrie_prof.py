#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ====================================================================
# Dosage conductimétrique MODELISATION                                 
# ====================================================================
"""
Programme permettant de représenter l'évolution des quantités de  
matière des espèces en fonction du volume de solution titrante versé
La seconde partie du programme permet de tracer la conductivité de   
la solution en fonction du volume de solution titrante versé
"""

"""
Réaction support de titrage A+B->C 
A : réactif titré   cA = ? , vA
B : réactif titrant cB , vEqv
En présence d'ions spectateurs :
SA: apportés par le réactif titré
SB: apportés par le réactif titrant
"""

# importe le module pyplot de matplotlib en le renommant plt
from matplotlib import pyplot as plt

# importe NumPy en la renommant np
import numpy as np

# Saisie des informations utiles concernant la réaction étudiée
print("\nLa réaction support de titrage est :  A + B -> C")

# Saisie des paramètres
vA= 100.0 # volume de solution titré en mL
cB= 0.01 # Concentration du réactif titrant en mol/L
vEqv= 13.3 #Volume de titrant versé à l'équivalence en mL

# Calcul de la concentration cA en réactif titré à partir de la 
# relation à l'équivalence : cAvA/1 = cBvEqv/1
cA= cB* vEqv / vA
# Affichage de la concentration en réactif titré 
print("la concentration du réactif titré est :",cA , "mol/L")

# Fonction calculant la composition du
# système avant l'équivalence : v<vEqv
def Avant_Eqv(v):
    nA = cA*vA-cB*v
    nB = 0
    nC = cB*v # quantité de matière de produit formé pour un volume v versé
    nSA = cA*vA
    nSB = cB*v
    return nA,nB,nC,nSA,nSB

# Fonction calculant la composition du
# système après l'équivalence (incluse) : v>=vEqv
def Apres_Eqv(v):
    nA = 0
    nB = cB*v - cA*vA
    nC = cA*vA
    nSA = cA*vA
    nSB = cB*v
    return nA,nB,nC,nSA,nSB

# Défintion du domaine des abscisses v volume versé de O à 2*vEqv avec 
# 21 valeurs régulièrement espacées (v en mL)
v = [i*vEqv/10 for i in range(21)]

# Initialisation des listes vides nA,nB,nC,nSA,nSB pour 
# les quantités de matière (en mmol)
nA,nB,nC,nSA,nSB = [],[],[],[],[]

# pour chaque valeur x du volume v de solution titrante versé
for x in v:                                           
    if x<vEqv:    # avant l'équivalence
        # Calcul des quantités de matière notées nA_x,nB_x,nC_x,
        # nSA_x,nSB_x (en mmol)
        nA_x,nB_x,nC_x,nSA_x,nSB_x = Avant_Eqv(x)   
        nA.append(nA_x)# insère la valeur nA_x en fin de liste nA
        nB.append(nB_x)# insère la valeur nB_x en fin de liste nB
        nC.append(nC_x)# insère la valeur nC_x en fin de liste nC
        nSA.append(nSA_x)# insère la valeur nSA_x en fin de liste nSA                            
        nSB.append(nSB_x)# insère la valeur nSB_x en fin de liste nSB    

    else:         # après l'équivalence
        # Calcul des quantités de matière notées nA_x,nB_x,nC_x,
        # nSA_x,nSB_x (en mmol)
        nA_x,nB_x,nC_x,nSA_x,nSB_x = Apres_Eqv(x)   
        nA.append(nA_x)# insère la valeur nA_x en fin de liste nA
        nB.append(nB_x)# insère la valeur nB_x en fin de liste nB
        nC.append(nC_x)# insère la valeur nC_x en fin de liste nC
        nSA.append(nSA_x)# insère la valeur nSA_x en fin de liste nSA                            
        nSB.append(nSB_x)# insère la valeur nSB_x en fin de liste nSB    


# Titre et initialisation de la fenêtre graphique
plt.figure('Quantité de matière',figsize=(7,8))     

# Tracé des courbes des quantités de matière en fonction de v
plt.plot(v,nA,label='$n_{A}=n_{titré}$')   # nA = ntitré = f(v)
plt.plot(v,nB,label='$n_{B}=n_{titrant}$') # nB = ntitrant = f(v)
plt.plot(v,nC,label='$n_{C}$')             # nC = f(v)
# Tracé en pointillé de la courbe nSA = nSpectateur-titré = f(v)
plt.plot(v,nSA,':',label='$n_{Spectateur-titré}$')        
# Tracé en pointillé de la courbe nSB = nSpectateur-titrant = f(v)
plt.plot(v,nSB,':',label='$n_{Spectateur-titrant}$')

# Label des axes
plt.xlabel('Volume $v$ de solution titrante versé (en mL)')
plt.ylabel('Quantité de matière des espèces (en mmol)')

# Limite des axes
y_max = max(max(nA),max(nB),max(nC),max(nSA),max(nSB))
plt.xlim(0,max(v)*1.1)
plt.ylim(0,y_max*1.2)

# Titre supérieur
plt.suptitle("Evolution des quantités de matière lors d'un titrage")

# Fonction affichant en titre la réaction support de titrage et les données
def titre(vA,cB,vEqv):
    reactifs = ' A$_{titré}$ + B$_{titrant}$'
    produits = ' C'
    equation = reactifs+'$ \longrightarrow $ '+produits
    data1 ='$v_{A}$ ='+str(vA)+' mL,  '
    data2 ='$c_{B}$ = '+'%.1e'%cB+' mol$\cdot$L$^{-1}$,  '
    data3 ='$v_{Eqv}$ ='+str(vEqv)+' mL'
    data=data1+data2+data3
    plt.title('Réaction support de titrage : '+equation+'\n'+data)
    return

titre(vA,cB,vEqv) # Affichage du titre (appel de la fonction titre)
plt.grid(ls='--') # Affichage d'une grille
plt.legend()      # Affichage de la légende
# plt.show()


# =========== Tracé de la courbe de suivi conductimétrique ===========
"""
Supprimer les guillemets des lignes 140 et 183 pour rendre actives les
lignes de code suivantes.
"""

    #Conductivités molaires ioniques en mS.m^2.mol^-1 :")
l_A= 7.6
l_B= 6.2
l_SA= 5.0
l_SB= 7.1
    # Conversions des listes v, nA, nB, nSA, nSB en tableaux 1D avec 
V= np.array(v)
N_A=np.array(nA)
N_B=np.array(nB)
N_SA=np.array(nSA)
N_SB=np.array(nSB)

    # Calcul de la conductivité du système sigma en S.m-1 
    # pour chaque valeur de V en mL (sigma : tableau à 1D)
    # Remarque : la conductivité est obtenue en S.m-1 en combinant  
    # les concentrations en mol.L-1 et les conductivités molaires 
    # ioniques en mS.m2.mol-1
sigma = (l_A*N_A + l_B*N_B +  l_SA*N_SA + l_SB*N_SB)/(V+vA)   

    # Titre et initialisation de la fenêtre graphique
plt.figure("Suivi conductimétrique", figsize=(7,8)) 
    
plt.plot(V,sigma,label='$\sigma$') # Tracé de sigma = f(V)      
    
    # Mise en forme de la fenêtre graphique
plt.suptitle("Suivi conductimétrique lors d'un titrage")
titre(vA,cB,vEqv) # Appel de la fonction titre (cf ligne 130-140)
plt.xlabel('Volume $v$ de solution titrante versé (en mL)')
plt.ylabel('$\sigma$ (en S$\cdot$m$^{-1})$')
plt.ticklabel_format(axis ='y',style='sci',scilimits=(-3,-3))
y_max = max(sigma)
plt.xlim(0,max(v)*1.1)
plt.ylim(0,y_max*1.2)
plt.grid(ls='--')
plt.legend()    
plt.show()
  