#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ====================================================================
# Cinétique loi de vitesse ordre 1                                   
# ====================================================================
import matplotlib.pyplot as plt
import numpy as np



# ====================================================================
# Etape 1 : Importation des données expérimentales
# ====================================================================

t,cE=[],[]  # Initialisation des listes pour t et la concentration
with open('..............txt') as fichier:
    header = [fichier.readline() for i in range(2)]
    line = fichier.readline().replace(',','.')
    while line:
        data0,data1,data2,data3=line.split('\t') 
        t.append(float(data0))      # Dates (en s)
        cE.append(float(data3))  # concentration     
        line=fichier.readline().replace(',','.')
        


N = len(t)  # Nombre de dates


# ====================================================================
# Etape 2 : Calcul de la vitesse volumique de consommation de E
# ====================================================================

vE=[] # Définition d'une liste vide pour la vitesse de consommation de E
for i in range(N-1) : 
    vEi= ....................... # vitesse de disparition de E
                                       # (en mol/L/s)  
    vE.append(vEi) 
    
# ====================================================================
# Etape 3 : Représentations graphiques
# ====================================================================
plt.figure(figsize=(12,12))
""" La figure présentera 2 graphes (subplot) sur 2 lignes et 2 colonnes :
plt.subplot(nb de lignes, nb de colonnes, index du graphe)."""
# ====================================================================
plt.subplot(2,2,1)      # Nuage de points [E] = f(t)

plt.plot(t,cE,'+', label = '$cE=f(t)$', clip_on=False)             
plt.xlabel("$t$ (en s)")   
plt.ylabel("$A$ (sans unité)")
plt.xlim(0,max(t))
plt.ylim(0,max(cE))
plt.grid(ls='--')
plt.legend(loc=9)                   

# ====================================================================
plt.subplot(2,2,3)      # Nuage de points vE = f(t)

del t[-1]       # Retire la dernière valeur de la liste t

plt.plot(t,vE,'c+', label = '$v_{d,E}=f(t)$', clip_on=False)
plt.xlabel("$t$ (en s)")   
plt.ylabel("$v_{d,E}$ (en mol$\cdot$L$^{-1}\cdot$s$^{-1}$)")
plt.ticklabel_format(axis='y',style='sci',scilimits=(0,0))             
plt.xlim(0,max(t))
plt.ylim(0,max(vE))
plt.grid(ls='--')                    
plt.legend(loc=9)                    

# ====================================================================
plt.subplot(2,2,2)      # Nuage de points vE = f([E])

del cE[-1]       # Retire la dernière valeur de la liste cE

c_E=np.array(cE) # Convertit la <liste> 'cE' en <tableau à 1D> 'c_E'

# modélise le nuage de points de coordonnées (x,y)
# par une droite d'équation y = ax+b
# renvoie un tableau [a,b]
a_1,b_1=np.polyfit(c_E,vE,1)

plt.plot(c_E,vE,'b+', label='$v_{d,E}=f([E])$', clip_on=False)
plt.plot(c_E,a_1*c_E+b_1,'r',label='Modélisation')
plt.xlabel("$[B]$ (en mol$\cdot$L$^{-1}$)")
plt.ylabel("$v_{C,B}$ (en mol$\cdot$L$^{-1}\cdot$s$^{-1}$)")
plt.ticklabel_format(axis='both',style='sci',scilimits=(0,0))             
plt.xlim(0,max(c_E))
plt.ylim(0,max(vE))
plt.grid(ls='--') 
plt.legend(loc=9)
print ("la courbe v=f([E] a pour coefficient directeur :",a_1) 
# ====================================================================
plt.subplot(2,2,4)      # Nuage de points vE = f([E]²)

a_2,b_2=np.polyfit(c_E**2,vE,1) # Modélisation de la forme y = a*x + b

plt.plot(c_E**2,vE,'b+',label='$v_{d,E}=f([E]^{2})$', clip_on=False)
plt.plot(c_E**2,a_2*(c_E**2)+b_2,'r',label='Modélisation')
plt.xlabel("$[E]^{2}$ (en mol$^{2}\cdot$L$^{-2}$)")             
plt.ylabel("$v_{d,E}$ (en mol$\cdot$L$^{-1}\cdot$s$^{-1}$)")
plt.ticklabel_format(axis='both',style='sci',scilimits=(0,0))             
plt.xlim(0,max(c_E**2))
plt.ylim(0,max(vE))
plt.grid(ls='--') 
plt.legend(loc=9)

plt.tight_layout()
plt.show()