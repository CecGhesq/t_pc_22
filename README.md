# T_PC_22


Bienvenue sur la plateforme de fichiers du groupe de terminale spécialité de Mme GHESQUIERE au lycée M de Flandre de Gondecourt
## Informations générales :

* Contact : cecile.ghesquiere@ac-lille.fr 
* [Lien vers le réseau du lycée](http://lyceemargueritedeflandre.online:8080/owncloud/index.php/login) : mettre vos identifiants comme au lycée
* Pour mettre sa CALCULATRICE EN MODE EXAMEN : [le tutoriel selon votre modèle ici](Calculatrices/mode_examen)
* ![](./img/35426.gif)--> vers les DS pour les retravailler (si si !) : [ici](./Evaluations) 
* lien vers des videos de cours [Ambroise Ravi](https://www.youtube.com/@raviambroise8647/playlists)

* lien vers le site [quiziniere soir](https://www.quiziniere.com/diffusions/24Z887) 


## Déroulé des chapitres  

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20869.gif)  
* # Chapitre 17 Mouvement des fluides
    * 🕯️[TP](./Acces_chapitres/C17_mvt_fluide/TP_BERNOUILLI_TORRICELLI.pdf)  
    * [cours](./Acces_chapitres/C17_mvt_fluide/C17_mvt_fluide_cours_eleve.pdf)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20868.gif)  
* # Chapitre 16 Piles
    * 🧪[TP](./Acces_chapitres/C16_piles/TP_Pile.pdf)  
    * [cours](./Acces_chapitres/C16_piles/C17_piles.pdf)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20867.gif)  
* # Chapitre 15 Optimisation de synthèse
    * [Révisions](./Acces_chapitres/C15_strategies_syntheses/prérequis_chap-optimisation_synthèse.pptx)
    * 🧪[TP](./Acces_chapitres/C15_strategies_syntheses/TP_C15_opt_synth.pdf)  
    * [cours](./Acces_chapitres/C15_strategies_syntheses/C15_cours_optimisation_synthese_el.pdf)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20866.gif)   

* # Chapitre 14  Transferts thermiques
    * [Cours](./Acces_chapitres/C14_transferts_thermiques/C14_cours_transferts_thermiques_eleves_2023.pdf)  
    * 🕯️[TP](./Acces_chapitres/C14_transferts_thermiques/TP_Détermination_capacité_thermique.pdf)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20865.gif)   

* # Chapitre 13  Equilibre Acide / base  
    * [Cours](./Acces_chapitres/C13_equilibre_AB/C13_Equilibre_reaction_acide_base_el.pdf) à compléter  
    * Activité 3 du livre p 162: [sujet](./Acces_chapitres/C13_equilibre_AB/C13_act3p162_ELEVE.pdf) et le [fichier Python](./Acces_chapitres/C13_equilibre_AB/ch07_162_standard.py) à télécharger  
    * 🧪 Activité 4 du livre : [sujet](./Acces_chapitres/C13_equilibre_AB/C13_act4p163_ELEVE.pdf) __corrigé dans le dossier du chapitre__  
    *💻 Activité 5 du livre : [sujet](./Acces_chapitres/C13_equilibre_AB/C13_act5p164_ELEVE.pdf)  et le [fichier Python](./Acces_chapitres/C13_equilibre_AB/act5.py) à télécharger  ; corrigé dans le dossier du chapitre 
    * exercice 25 ; [fichier Python](./Acces_chapitres/C13_equilibre_AB/ex25.py) à télécharger ( très proche de l'activité 5!)
    * sujet Bac : [Solution désinfectante](./Acces_chapitres/C13_equilibre_AB/2022-Amsud-J1-Exo1-Sujet-AcideLactique-10pts_0-1.pdf) Amérique du Sud 2022   

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20864.gif)  

* # Chapitre 12  Atténuations des ondes sonores
    * Révision : [diaporama](./Acces_chapitres/C12_attenuations/prerequis_chap_Attenuation_sonore.pptx)  
    * Vidéo support de cours [à voir](./Acces_chapitres/C12_attenuations/L_I.mp4) pour compléter le cours [ici](./Acces_chapitres/C12_attenuations/C12_attenuation_el.pdf) COURS COMPLETE : [ici](./Acces_chapitres/C12_attenuations/C12_attenuation_prof.pdf)
    * TP Atténuations des ondes sonores : [sujet](./Acces_chapitres/C12_attenuations/TP_attenuation_el.pdf)


![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) 

* ## Chapitre 11 : Diffraction Interférences  
    * [Activité préparatoire](./Acces_chapitres/C11_diffraction_interferences/cours%20diffraction-interf%C3%A9rences/Activit%C3%A9%20d_exploration-Diffraction%20%C3%A9l%C3%A8ve.pdf)   et 🎥 vidéo [à télécharger](./Acces_chapitres/C11_diffraction_interferences/cours%20diffraction-interf%C3%A9rences/FLUVORE_cuve_a_ondes_0c60b.avi)
    * cours [poly à compléter](./Acces_chapitres/C11_diffraction_interferences/cours%20diffraction-interf%C3%A9rences/11_%20Diffraction_interference_el.pdf) 

    |||
    |---|---|
    |[Ostralo](http://physique.ostralo.net/diffraction_interference/) |![](./img/diffraction.jpg)|
    |1.2 Diffraction à la surface del'eau|[video](./Acces_chapitres/C11_diffraction_interferences/cours diffraction-interférences/animations/1_2_Tulloue_diffraction.mp4)|
    |1.3 Diffraction d'onde mumineuse|[video](./Acces_chapitres/C11_diffraction_interferences/cours diffraction-interférences/animations/1_3_diffraction_lumiere.mp4)|
    |2.1 Croisement des ondes|[video](./Acces_chapitres/C11_diffraction_interferences/cours diffraction-interférences/animations/2_1_croisement ondes.mp4)|
    |2.3 Interferences d'ondes à la surface de l'eau|[video](./Acces_chapitres/C11_diffraction_interferences/cours diffraction-interférences/animations/2_3_constr_destr_Tulloue.mp4)|
    |2.4 différence de marche|[animation](https://physique-chimie.ac-normandie.fr/spip.php?article203)|
    
 
    * 🕯️ TP : Mesure du diamètre d'un cheveu par diffraction  [sujet](./Acces_chapitres/C11_diffraction_interferences/TP_Diffraction.pdf)  
    * 💻 Activité Python : superposition d'ondes : [sujet](./Acces_chapitres/C11_diffraction_interferences/TP_Interf%C3%A9rences_const-dest.pdf) ; fichier [python](./Acces_chapitres/C11_diffraction_interferences/somme%20de%202%20signaux%20%C3%A9l%C3%A8ve.Py)  
    * 🕯️ TP : Interférences lumineuses [sujet](./Acces_chapitres/C11_diffraction_interferences/TP_interferences_lum.pdf) avec le fichier [Python](./Acces_chapitres/C11_diffraction_interferences/int_lum_el.Py) à télécharger 

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif) ![](https://www.icone-gif.com/gif/chiffres/violet/ecole_chiffre01.gif)  
* ## Chapitre 10 : Analyse chimique d'un système
    * révisions : [Video](./Acces_chapitres/C10_analyse_meth_chimiques/revision/titrage_rev.mp4) et [poly](Acces_chapitres/C10_analyse_meth_chimiques/revision/rev_tit_color.pdf)
    * 🧪 TP1 : __Titrage avec suivi pH-métrique__ [sujet](./Acces_chapitres/C10_analyse_meth_chimiques/10_TP_titrage_pH.pdf) ; les courbes sont [ici](./Acces_chapitres/courbe_titrage_détartrant.pdf)
    * Cours : Analyse chimique d'un système [poly à compléter](/Acces_chapitres/C10_analyse_meth_chimiques/10_fiche élève.pdf)
    * 🧪 TP2 : __Titrage avec suivi conductimétrique__ [sujet](./Acces_chapitres/C10_analyse_meth_chimiques/10_TP2_titrage_cond_suivi_python.pdf) ; le script Python à compléter [à télécharger](./Acces_chapitres/C10_analyse_meth_chimiques/10_TP2_titrage_cond_suivi_python.py) ;  les courbes sont [ici](./Acces_chapitres/C10_analyse_meth_chimiques/courbes_Titrage_conductimétrique.pdf)  💻 et [ici](./Acces_chapitres/C10_analyse_meth_chimiques/Courbes_Python.pdf) avec Python  ou celui corrigé [ici](./Acces_chapitres/C10_analyse_meth_chimiques/conductimetrie_prof.py)  
 -->  lien vers les sujets de bac sur le titrage : [ici](https://labolycee.org/thema-dosage-par-titrage)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20871.gif)  
* ## Chapitre 9 : Mouvement des satellites et des planètes
    * Cours : [document à compléter du chapitre](./Acces_chapitres/C9_satellites/9_satellite_doc_eleves.pdf)
    * Animations :  
    
    |||
    |:---:|:---:|
    |[mouvement circulaire](https://phet.colorado.edu/sims/html/gravity-and-orbits/latest/gravity-and-orbits_fr.html)| ![](./img/satellite.jpg)|
    |[lois de Kepler](http://www.jf-noblet.fr/kepler2/index.htm)|![](./img/lois_de_Kepler.jpg)| 

    * 🪐 TP [dossier à télécharger et à dézipper](./Acces_chapitres/C9_satellites/dossier_eleve.7z) et [sujet](./Acces_chapitres/C9_satellites/TP_Kepler_eleve.pdf)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20870.gif)  
* ## Chapitre 8 : Evolution de la transformation chimique
    * cours [poly à compléter](./Acces_chapitres/C8_evolution_transf/C8_evol.pdf)  
    * 🧪 [TP](./Acces_chapitres/C8_evolution_transf/C8_TP.pdf)  

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20869.gif)  
* ## Chapitre 7 : Systèmes capacitifs
    * cours [poly à compléter](./Acces_chapitres/C7_RC/C7_cours_eleve.pdf)   
    * 🔌 TP [sujet](./Acces_chapitres/C7_RC/C7_TP_eleve.pdf) ; [fichier python](echantillonage.py) à utiliser dans Mu ; fichier de l'atelier scientifique pour la [décharge](./Acces_chapitres/C7_RC/decharge.lab) à exploiter. 💻
    * sujets de bac prévus pour le DS 3 :
        * [capteur capacitif](https://labolycee.org/un-capteur-capacitif)  
        * [supercondensateur](https://labolycee.org/les-supercondensateurs)  
        * [micro-accéléromètre](https://labolycee.org/un-microaccelerometre-capacitif)  
        * [défibrillateur cardiaque](https://labolycee.org/defibrillateur-cardiaque-0)
   
![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20868.gif)  
* ## Chapitre 6 : Suivi temporel macroscopique d'un système chimique  
    * cours [poly à compléter](./Acces_chapitres/C6_suivi_temporel_macro/6_suivi_macros_eleve.pdf)   
    * Révisions oxydo-réduction : [fiche](./Acces_chapitres/C6_suivi_temporel_macro/rev_oxydo-red.jpg) et [video](./Acces_chapitres/C6_suivi_temporel_macro/REVISION_Oxydoreduction.mp4)
    * 🧪 TP 1 : les facteurs cinétiques : [sujet](./Acces_chapitres/C6_suivi_temporel_macro/TP/TP_fact_cinétique/6_TP_Facteurs_cinétiques.pdf) + notice du colorimètre [ici](./Notices_manip/notice_Colorimètre_filtre.pdf)  
    * 🧪 TP2 : Vitesse et loi cinétique d'ordre 1 [sujet](./Acces_chapitres/C6_suivi_temporel_macro/TP/TP_vitesse/TP_vitesse.pdf) et le fichier python à [télécharger](./Acces_chapitres/C6_suivi_temporel_macro/TP/TP_vitesse/cinetique_loi_1_ELEVE.py)  ![](https://framagit.org/CecGhesq/t_spe_pc_21/-/blob/main/img/330px-Python.jpg)

    
![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20867.gif)  
* ## Chapitre 5 : Mouvement dans un champ uniforme ; Energies  
    * cours [poly à compléter](./Acces_chapitres/C5_Mouvement_champ/5_mouvements_champ_energies_eleve.pdf)  avec le [diaporama](./Acces_chapitres/C5_Mouvement_champ/mvt_para.ppsx)  
    * Révisions énergies cinétique, potentielle,mécanique : [fiche](./Acces_chapitres/C5_Mouvement_champ/révision_fiche_synthèse1ere.pdf) et [video](./Acces_chapitres/C5_Mouvement_champ/REVISION_Theoreme-de-l-energie-cinetique.mp4)
    * 🏹 TP : modélisation du mouvement parabolique [sujet](./Acces_chapitres/C5_Mouvement_champ/TP mouvement parabolique_energies_eleve.pdf)  avec la [video](./Acces_chapitres/C5_Mouvement_champ/vidéo_mvt_para.avi)  ; ![](https://framagit.org/CecGhesq/t_spe_pc_21/-/blob/main/img/330px-Python.jpg)fichier python à compléter : [ici](./Acces_chapitres/C5_Mouvement_champ/energie_python.py) ; mettre dans le meme dossier ce [fichier de relevés](./Acces_chapitres/C5_Mouvement_champ/relevé_points_mod.txt) de points et l'appeler avec le bon intitulé dans le fichier python à la ligne (with open ......) ; ![](https://framagit.org/CecGhesq/t_spe_pc_21/-/blob/main/img/330px-Python.jpg) 💻

    
![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20866.gif)  
* ## Chapitre 4 : Forces et mouvements  
    * Prérequis : [video](./Acces_chapitres/C4_forces_mouvements/REVISION_Vecteur-variation-de-vitesse.mp4) et le pdf de révision sur [la dérivée](./Acces_chapitres/C4_forces_mouvements/pré-requis-derivee_point_Maths.pdf)  
    * cours [classe inversée](./Acces_chapitres/C4_forces_mouvements/chap_classe_inv.pdf) 
    * 🏹 TP :
        * [sujet](./Acces_chapitres/C4_forces_mouvements/TP_mvt_projectile.pdf)
        * [chronophotographie](./Acces_chapitres/C4_forces_mouvements/chrono_élève.pdf) 💻
        * [dossier](./Acces_chapitres/C4_forces_mouvements/TP_dossier_eleve)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20865.gif)  
* ## Chapitre 3 : Analyse physique
    * prérequis : voir le [diaporama](./Acces_chapitres/C3_analyse_physique/prerequis_chap_analyse_syst_phys.ppsx) ou [version pdf](./Acces_chapitres/C3_analyse_physique/prerequis_chap_analyse_syst_phys.pdf) : 
    * cours [à compléter](./Acces_chapitres/C3_analyse_physique/C3_a_comp.pdf)
    * 🧪 TP 1 [Mesure de conductances](./Acces_chapitres/C3_analyse_physique/TP_Conductance_.pdf)  
    * 🧪 TP 2 [dosage par étalonnage](./Acces_chapitres/C3_analyse_physique/ch02_50_fiche_eleve_stand.pdf) et les documents complémentaires : [ici](./Acces_chapitres/C3_analyse_physique/doc_TP2.jpg)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20864.gif)    
* ## Chapitre 2 Transformations acide/base pH
    * Révisions : [diaporama](./Acces_chapitres/C2_Acide_base/prerequis_AB.ppsx) et [vidéo à regarder](./Acces_chapitres/C2_Acide_base/REVISION_schema-Lewis.mp4)
    * Activité [à compléter](./Acces_chapitres/C2_Acide_base/act_AB.pdf)  
    * cours [à compléter](./Acces_chapitres/C2_Acide_base/2_transfAB_pH_el.pdf)  
    *  🧪 TP : [mesures de pH](./Acces_chapitres/C2_Acide_base/TP_Mesur_pH.pdf)

![](https://images.toucharger.com/img/graphiques/gifs_animes/chiffres/serie_9/serie_9.20863.gif)
* ## Chapitre 1 : La lunette astronomique 
    * __Révisions__ :  
        * [vidéo](./Acces_chapitres/C1_lunette_astronomique/revisions/REVISION_formation-image.mp4) à visionner ;  
        * regarder le [diaporama](./Acces_chapitres/C1_lunette_astronomique/revisions/prerequis_chap_lunette_astronomique.ppsx) ;  
        * réaliser la feuille d'[exercices](./Acces_chapitres/C1_lunette_astronomique/revisions/rev_lentilles_exos.pdf)  

    * __cours__ [à compléter](./Acces_chapitres/C1_lunette_astronomique/cours/C1_lunette_cours_eleve.pdf)

    * __Activité__ : [sujet](./Acces_chapitres/C1_lunette_astronomique/activité/lun_com.pdf)

    * __exercices__ : 15, 36, 39, 42, 44, 47, 49 p 456
        
    * 🔭  __TP__ sur la [lunette astronomique](./Acces_chapitres/C1_lunette_astronomique/TP/TP_lunette.pdf)  

    * ![](https://www.gif-maniac.com/gifs/53/52839.gif) [La lunette de Meudon](https://labolycee.org/la-grande-lunette-de-meudon)   
    ------
*  [Révisions de Noel](./Evaluations/revision_vacances_noel.md)     
![](./img/53630_r.gif)  __Révisions vacances de la Toussaint :__ ![](./img/200.webp)
* mouvement dans le champ électrique : https://labolycee.org/determination-du-rapport-em-pour-lelectron
* mouvement dans le champ de pesanteur : https://labolycee.org/etude-du-vol-dune-balle-de-golf
* cinétique chimique :
    *  __partie 2__ de  https://labolycee.org/le-parfum-de-la-poire
    * https://labolycee.org/synthese-du-2-methylpropan-2-ol : sujet avec l'équation différentielle vue dans le chapitre 7 (dipôle RC) , permet de faire le lien ... 

## Méthodes indispensables :

* Corrigés des méthodes indispensables :  

|          |          |          |          |          |          |
|----------|----------|----------|----------|----------|----------|
| [fiche_1](./methodes_indispensables/1_corrige.pdf)  | [fiche_2](./methodes_indispensables/2_corrige.pdf)  | [fiche_3](./methodes_indispensables/3_corrige.pdf)  | [fiche_4](./methodes_indispensables/4_corrige.pdf)  | [fiche_5](./methodes_indispensables/5_corrige.pdf)  | [fiche_6](./methodes_indispensables/6_corrige.pdf) |
| [fiche_7](./methodes_indispensables/7_corrige.pdf)  | [fiche_8](./methodes_indispensables/8_corrige.pdf)  | [fiche_9](./methodes_indispensables/9_corrige.pdf)  | [fiche_10](./methodes_indispensables/10_corrige.pdf) | [fiche_11](./methodes_indispensables/11_corrige.pdf) | [fiche_12](./methodes_indispensables/12_corrige.pdf) |
| [fiche_13](./methodes_indispensables/13_corrige.pdf) | [fiche_14](./methodes_indispensables/14_corrige.pdf) | [fiche_15](./methodes_indispensables/15_corrigé.pdf) | [fiche_16](./methodes_indispensables/16_corrige.pdf)| [fiche_17](./methodes_indispensables/17_corrige.pdf) | [fiche_18](./methodes_indispensables/18_corrige.pdf) |
| [fiche_19](./methodes_indispensables/19_corrige.pdf) | [fiche_20](./methodes_indispensables/20_corrige.pdf) | [fiche_21](./methodes_indispensables/21_corrige.pdf) | [fiche_22](./methodes_indispensables/22_corrige.pdf) | [fiche_23](./methodes_indispensables/23_corrige.pdf) | [fiche_24](./methodes_indispensables/24_corrige.pdf) |  

    
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Ce dépôt est sous licence  <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
